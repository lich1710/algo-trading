# -*- coding: utf-8 -*-
"""
Created on Tue Nov 05 23:28:43 2019

@author: Trinh Duc Linh
"""

""" downloading price volume data """

import datetime
import zipfile
import csv
import StringIO
from io import BytesIO
import requests
import sys, traceback
import logging 

date1 = datetime.datetime.today().strftime('%Y%m%d')
date2 = datetime.datetime.today().strftime('%d%m%Y')

try:
    url = 'http://images1.cafef.vn/data/' + date1 + '/CafeF.SolieuGD.Raw.' + date2 + '.zip' 
    response = requests.get(url, allow_redirects=True)
    zipDocument = zipfile.ZipFile(BytesIO(response.content))
    path = "C:/Work/VN Stock/vn_price/Daily/" + date1
    zipDocument.extractall(path=path)
	
    url = 'http://images1.cafef.vn/data/' + date1 + '/CafeF.SolieuGD.' + date2 + '.zip' 
    response = requests.get(url, allow_redirects=True)
    zipDocument = zipfile.ZipFile(BytesIO(response.content))
    path = "C:/Work/VN Stock/vn_price/Daily/" + date1
    zipDocument.extractall(path=path)
    
except:
    print "Trigger Exception, traceback info forward to log file."
    error_log_path = "C:/Work/VN Stock/vn_price/Daily/" + '/errlog' + date1 + '.txt'
    traceback.print_exc(file=open(error_log_path,"a"))
    f = open(error_log_path, "a")
    f.write('Response content\n')
    f.write(response.content)
    f.close()
    sys.exit(1)