#!/usr/bin/env bash
set -e

dir="$(pwd)"
me="$(basename "$0")"

parse_args() {
  # Set args from a local environment file.
  if [ -e ".env" ]; then
    source .env
  fi

  # Parse arg flags, if something is exposed as an environment variable,
  # set/overwrite it here. Otherwise, overwrite the internal variable instead.
  while :; do
    if [[ $1 == "-h" || $1 == "--help" ]]; then
      echo "$help_message"
      return 0
    elif [[ $1 == "-v" || $1 == "--verbose" ]]; then
      VERBOSE=true
      shift
    elif [[ ($1 == "-c" || $1 == "--container-name") && -n $2 ]]; then
      CONTAINER_NAME=$2
      shift 2
    else
      break
    fi
  done
}

main() {
  parse_args "$@"

  if [[ ! "${CONTAINER_NAME}" ]]; then
    echo "[INFO] Specify container name by using either -c or --container-name option..."
    exit 0
  fi

  TOKEN="$(docker container logs "${CONTAINER_NAME}" | sed -n -e 's/^.*token=//p' | head -1)"

  echo "${TOKEN}"

  open "http://$(docker-machine ip default):8888/tree?token=${TOKEN}"
}

[[ $1 == --source-only ]] || main "$@"
