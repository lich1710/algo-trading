#!/usr/bin/env bash
set -e

dir="$(pwd)"
me="$(basename "$0")"

parse_args() {
  # Set args from a local environment file.
  if [ -e ".env" ]; then
    source .env
  fi

  # Parse arg flags, if something is exposed as an environment variable,
  # set/overwrite it here. Otherwise, overwrite the internal variable instead.
  while :; do
    if [[ $1 == "-h" || $1 == "--help" ]]; then
      echo "$help_message"
      return 0
    elif [[ $1 == "-v" || $1 == "--verbose" ]]; then
      VERBOSE=true
      shift
    elif [[ ($1 == "-c" || $1 == "--container-name") && -n $2 ]]; then
      CONTAINER_NAME=$2
      shift 2
    else
      break
    fi
  done
}

main() {
  parse_args "$@"

  if [[ ! "${CONTAINER_NAME}" ]]; then
    echo "[INFO] Specify container name by using either -c or --container-name option..."
    exit 0
  fi

  if [[ $(docker container ls --quiet --filter=name="${CONTAINER_NAME}" | wc -l) -ne 0 ]]; then
    echo "[INFO] Remove existing container ${CONTAINER_NAME}..."
    docker container stop "${CONTAINER_NAME}" && docker container rm --force "${CONTAINER_NAME}"
  fi

  docker container run \
          --name jupyter \
          --tty \
          --detach \
          --privileged \
          --env TZ=Asia/Singapore \
          --env JUPYTER_ENABLE_LAB=yes \
          --volume "${dir}":/home/jovyan/work \
          --publish 8888:8888 \
          jupyter/minimal-notebook:latest
}

[[ $1 == --source-only ]] || main "$@"
