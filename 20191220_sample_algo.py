# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

# +
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import alphaOp
import tsOp
import nanOp
import simulate
import cPickle as pickle

# +
# Define constant

bookSize = 20e6
# -

# ==================================
# # Read and check data

# ### Read file

### checking data
filename1 = 'CafeF.RAW_HSX.Upto01.11.2019.csv'
filename2 = 'CafeF.RAW_HNX.Upto01.11.2019.csv'

df1 = pd.read_csv(filename1)
df1['Exchange'] = 'HSX'
df2 = pd.read_csv(filename2)
df2['Exchange'] = 'HNX'

df = df1.append(df2)

# Length of the data
len(df)

df = df.rename(columns={"<Ticker>": "Ticker", "<DTYYYYMMDD>": "Date", "<Open>": "Open","<High>": "High","<Low>": "Low","<Close>": "Close", "<Volume>": "Volume"})

# Get unique tickers
tickers = np.unique(df.Ticker.values).astype(str)
# filter for stock ticker only
stock_tickers = []
for ticker in tickers:
    if len(ticker) == 3:
        stock_tickers.append(ticker)
stock_tickers = np.array(stock_tickers)
print 'Number of stock ticker is: ', len(stock_tickers)

# Get stock df
stock_df = df[df.Ticker.isin(stock_tickers)]

close_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Close')
open_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Open')

close_m = close_df.values
open_m  = open_df.values

# ### Verify data

# Quick plot close_m to check for Nun value
plt.plot(np.sum(np.isfinite(close_m), axis=1))
plt.show()

# Quick plot open_m to check for Nun value
plt.plot(np.sum(np.isfinite(open_m), axis=1))
plt.show()

dates  = np.unique(close_df.index)
print 'Number of unique dates is: ', len(dates)

# ==================================
# # Alpha
# * this is only demo, so it is very unrealistic:
#     * it does not incorporate T+3
#     * it does not incorporate any trading cost
# * book size is keep at fixed 20 mil everyday

# Get day to day percentage change
return_m = np.full_like(close_m, np.nan)
return_m[1:] = close_m[1:]/ close_m[:-1] - 1

# Get day to day percentage change
open_return_m = np.full_like(open_m, np.nan)
open_return_m[1:] = open_m[1:]/ open_m[:-1] - 1

# ## Reversion strategy

# ### Create Alpha
# * Alpha based on reversion of past n days.

# Get moving average
import numpy.matlib
alpha = tsOp.movingAverage(-return_m, 3)

# rank by row then normalize
# Formula: max alpha has rank=1, rank = alpha/max_alpha
alpha = alphaOp.groupRank(alpha)

# Neutralize by (Rank - Mean Rank of Row)
# thus sum each row =0, 0 dollar exposure
alpha = alphaOp.groupNeutralize(alpha)

# ### Simulate

# #### Both Long & Short

# +
# Copy alpha to a temp alpha to simulate
final_alpha = alpha.copy()

# Alpha calculated at end of the day for buy/sell of next day.
# Thus need to shift
final_alpha[1:]= final_alpha[:-1]

# Scale alpha to 20 million booksize
final_alpha = alphaOp.scaleAll(final_alpha, bookSize, nanIsZero=True)
# -

# Plot exposure each day
plt.plot(np.nansum(final_alpha, axis=1))
plt.show()

# plot booksize each day by sum of absolute value
plt.plot(np.nansum(np.fabs(final_alpha), axis=1))
plt.show()

# +
# Simulate our strategy
# Use date from 2009
simStartDi = np.where(dates > 20090101)[0][0]

pnl, tvr = simulate.fastSim(final_alpha[simStartDi:],
                            return_m = return_m[simStartDi:],
                            buying_fee = 0./100,
                            selling_fee = 0./100)

alphaOp.alphaStatSummary(final_alpha[simStartDi:],
                         pnl, tvr,
                         dates[simStartDi:],
                         bookSize=bookSize,
                         isAnnualSummary = True,
                         plot=True)
# -
# #### Only long


# +
# Copy alpha
final_long_alpha = alpha.copy()
# Remove negative (short)
final_long_alpha[final_long_alpha < 0] = 0

# Alpha calculated at end of the day for buy/sell of next day.
# Thus need to shift
final_long_alpha[1:]= final_long_alpha[:-1]

# Scale alpha to 20 million booksize
final_long_alpha = alphaOp.scaleAll(final_long_alpha, bookSize, nanIsZero=True)
# -

# Plot exposure each day
plt.plot(np.nansum(final_long_alpha, axis=1))
plt.show()

# plot booksize each day
plt.plot(np.nansum(np.fabs(final_long_alpha), axis=1))
plt.show()

# +
# Simulate our strategy
# Use date from 2009
simStartDi = np.where(dates > 20090101)[0][0]

pnl, tvr = simulate.fastSim(final_long_alpha[simStartDi:],
                            return_m = return_m[simStartDi:],
                            buying_fee = 0./100,
                            selling_fee = 0./100)

alphaOp.alphaStatSummary(final_long_alpha[simStartDi:],
                         pnl, tvr,
                         dates[simStartDi:],
                         bookSize=bookSize,
                         isAnnualSummary = True,
                         plot=True)
# -

# ## Momentum strategy

# ### Create alpha
# * calculate last 250 days average return, excluding last 20 days due to 20 day reversion effect
# * in general stocks exhibit short term reversion but long term momentum

alpha1 = tsOp.movingSum(tsOp.delay(return_m, 20), 230)

# rank 
alpha1 = alphaOp.groupRank(alpha1)

# Neutralize for 0 dollar exposure
alpha1 = alphaOp.groupNeutralize(alpha1)

# ### Simulate

# #### Both long and short

# +
# Copy alpha to a temp alpha to simulate
final_alpha1 = alpha1.copy()

# Alpha calculated at end of the day for buy/sell of next day.
# Thus need to shift
final_alpha1[1:]= final_alpha1[:-1]

# Scale alpha to 20 million booksize
final_alpha1 = alphaOp.scaleAll(final_alpha1, bookSize, nanIsZero=True)
# -

# Plot exposure each day
plt.plot(np.nansum(final_alpha1, axis=1))
plt.show()

# plot booksize each day by sum of absolute value
plt.plot(np.nansum(np.fabs(final_alpha1), axis=1))
plt.show()

# +
# Simulate our strategy
# Use date from 2009
simStartDi = np.where(dates > 20090101)[0][0]

pnl, tvr = simulate.fastSim(final_alpha1[simStartDi:],
                            return_m = return_m[simStartDi:],
                            buying_fee = 0./100,
                            selling_fee = 0./100)

alphaOp.alphaStatSummary(final_alpha1[simStartDi:],
                         pnl, tvr,
                         dates[simStartDi:],
                         bookSize=bookSize,
                         isAnnualSummary = True,
                         plot=True)
# -

# #### Only long

# +
# Copy alpha
final_long_alpha1 = alpha1.copy()
# Remove negative (short)
final_long_alpha1[final_long_alpha1 < 0] = 0

# Alpha calculated at end of the day for buy/sell of next day.
# Thus need to shift
final_long_alpha1[1:]= final_long_alpha1[:-1]

# Scale alpha to 20 million booksize
final_long_alpha1 = alphaOp.scaleAll(final_long_alpha1, bookSize, nanIsZero=True)
# -

# Plot exposure each day
plt.plot(np.nansum(final_long_alpha1, axis=1))
plt.show()

# Plot booksize each day
plt.plot(np.nansum(np.fabs(final_long_alpha1), axis=1))

# +
# Simulate our strategy
# Use date from 2009
simStartDi = np.where(dates > 20090101)[0][0]

pnl, tvr = simulate.fastSim(final_long_alpha1[simStartDi:],
                            return_m = return_m[simStartDi:],
                            buying_fee = 0./100,
                            selling_fee = 0./100)

alphaOp.alphaStatSummary(final_long_alpha1[simStartDi:],
                         pnl, tvr,
                         dates[simStartDi:],
                         bookSize=bookSize,
                         isAnnualSummary = True,
                         plot=True)
# -

# * momentum is a longer term strategy compared to reversion
# * that's why it has way lower turnover (<10%) compared to reversion turnover (> 70%)
# * since it has low turnover, using close-to-close return or open-to-open return did not make much difference to the result
# because it is not very sensitive to small daily changes (unlike reversion)
#
# * a possible idea could be combining this 2 signals together to diversify and achieve mid level turnover level

# ## Combined alpha

# * combining reversion and momentum alpha: just simple equal weight 2 portfolio

# #### Combined short and long

combined_alpha = final_alpha + final_alpha1

# Scale to bookSize
combined_alpha = alphaOp.scaleAll(combined_alpha, bookSize, nanIsZero=True)

# +
# Simulate
simStartDi = np.where(dates > 20090101)[0][0]

pnl, tvr = simulate.fastSim(combined_alpha[simStartDi:],
                            return_m = return_m[simStartDi:],
                            buying_fee = 0./100,
                            selling_fee = 0./100)

alphaOp.alphaStatSummary(combined_alpha[simStartDi:],
                         pnl, tvr,
                         dates[simStartDi:],
                         bookSize=bookSize,
                         isAnnualSummary = True,
                         plot=True)
# -

# #### Combined long only

combined_long_alpha = final_long_alpha + final_long_alpha1

# Scale to bookSize
combined_long_alpha = alphaOp.scaleAll(combined_long_alpha, bookSize, nanIsZero=True)

# +
# Simulate
simStartDi = np.where(dates > 20090101)[0][0]

pnl, tvr = simulate.fastSim(combined_long_alpha[simStartDi:],
                            return_m = return_m[simStartDi:],
                            buying_fee = 0./100,
                            selling_fee = 0./100)

alphaOp.alphaStatSummary(combined_long_alpha[simStartDi:],
                         pnl, tvr,
                         dates[simStartDi:],
                         bookSize=bookSize,
                         isAnnualSummary = True,
                         plot=True)
# -

# * more ideas ?? 
#  * technical analysis
#  * using order book data (best bid/ best ask)
#  * using volume to detect
#  * trade faster? using intraday data to incorprate in alpha (but where to get data??)
#  
# * more to consider:
#  * T+3 and cost
#  * liquidity of each stock (probably cannot trade all 800 stocks like this, need to choose top liquidity)
#  * minimum lot per stock to trade
#  * volume constraint: we cannot trade, for example, 50% of the daily volume of a stock, it will lead to impact (getting bad
#  price, revealing intention, etc), so we should constraint it to 5-10% of daily volume (approx by last 20 days average)


