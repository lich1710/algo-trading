# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %matplotlib inline

import alphaOp  # library for position matrix operation
import tsOp  # library for time-series operation such as moving average
import nanOp # libarry for dealing with NaN values
import simulate # simulator to simulate and show back-test result
import cPickle as pickle

# ## Read and check data

# Read raw data file
raw_data_dict = pickle.load( open( "pickle_raw_data_dict.p", "rb" ) )

open_df = raw_data_dict['Open']
high_df = raw_data_dict['High']
low_df = raw_data_dict['Low']
close_df = raw_data_dict['Close']
volume_df = raw_data_dict['Volume']

dates = np.unique(open_df.index)
len(dates)

# Convert data to float
# .values only return the value, axes label will be removed
open_m = open_df.values.astype(float)
high_m = high_df.values.astype(float)
low_m = low_df.values.astype(float)
close_m = close_df.values.astype(float)
volume_m = volume_df.values.astype(float)

# Verify close price, check for non trading day data
# .isfinite test the element, = true if not infinity or not Not a Number, return a boolean array
plt.plot(np.sum(np.isfinite(close_m),axis=1))
plt.show()

# Check the covereage of the data each day position has 0 data
coverage_v = np.sum(np.isfinite(close_m),axis=1)
np.where(coverage_v == 0)

coverage_v = np.sum(np.isfinite(open_m),axis=1)
np.where(coverage_v ==0)

# ### Check a stock with actual price from CAFEF
# - compare with actual price from CAFEF to see if data is correct
# - chose BMC because it moved a lot and was involved in stock split at the beginning

plt.plot(close_df['BMC'].values.astype(float))

# ## Get day-to-day return

# Daily close to close return
# Check for NAN
close_return_df = close_df.pct_change()

# daily open to open return
open_return_df = open_df.pct_change()

# daily close to close return
return_m = np.full_like(close_m, np.nan)
return_m[1:] = close_m[1:]/ close_m[:-1] - 1

# daily open to open return
open_return_m = np.full_like(open_m, np.nan)
open_return_m[1:] = open_m[1:]/ open_m[:-1] - 1

# ## Try some simple strategy
# * this is only demo, so it is very unrealistic:
#     * it does not incorporate T+3
#     * it does not incorporate any trading cost
# * book size is keep at fixed 20 mil everyday

# ### reversion strategy

# calculate last 5 days average return
import numpy.matlib
alpha = tsOp.movingAverage(-return_m, 3)

return_m

alpha

# rank by row then normalize, top=1, other = other/top
alpha = alphaOp.groupRank(alpha)

# Neutralize by Rank - Mean of Row, sum each row =0
# 0 dollar exposure
alpha = alphaOp.groupNeutralize(alpha)

np.nansum(alpha, axis=1)

# * assume we can long/short 

# +
final_alpha = alpha.copy()
universe_m = np.ones(final_alpha.shape).astype(bool)

# scale to 20 mil book
final_alpha = alphaOp.scaleAll(final_alpha,
                               20e6,
                               universe_m = universe_m,
                               nanIsZero= True)

# +
# test from 2009 as data coverage was too low before that (look at concent50 and 90)
simStartDi = np.where(dates >= 20090101)[0][0]

# delay the alpha position 1 day, because we get end of day information at the end of today trading, 
# but we can only trade/ build position tomorrow and realize the return after tomorrow

final_alpha[1:] = final_alpha[:-1]

# +
pnl, tvr = simulate.fastSim(final_alpha[simStartDi:],
                            return_m = return_m[simStartDi:])

alphaOp.alphaStatSummary(final_alpha[simStartDi:],
                         pnl,tvr,
                         dates[simStartDi:],
                         bookSize=20e6)

# +
# if we build all position in the open auction instead
pnl, tvr = simulate.fastSim(final_alpha[simStartDi:],
                            return_m = open_return_m[simStartDi:])

alphaOp.alphaStatSummary(final_alpha[simStartDi:],
                         pnl,tvr,
                         dates[simStartDi:],
                         bookSize=20e6)
# -

# * if we long only (get rid of the negative position)

# +
final_long_alpha = alpha.copy()
final_long_alpha[final_long_alpha < 0] = 0
universe_m = np.ones(final_long_alpha.shape).astype(bool)

# scale to 20 mil book
final_long_alpha = alphaOp.scaleAll( final_long_alpha, 20e6, universe_m = universe_m, nanIsZero= True )

# +
# test from 2009 as data coverage was too low before that (look at concent50 and 90)
simStartDi = np.where(dates >= 20090101)[0][0]

# delay the alpha position 1 day, because we get end of day information at the end of today trading, 
# but we can only trade/ build position tomorrow and realize the return after tomorrow

final_long_alpha[1:] = final_long_alpha[:-1]
# -

pnl, tvr, _, _ = simulate.fastSim(final_long_alpha[simStartDi:], return_m = return_m[simStartDi:])
alphaOp.alphaStatSummary(final_long_alpha[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# if we build all position in the open auction instead
pnl, tvr, _, _ = simulate.fastSim(final_long_alpha[simStartDi:], return_m = o2o_return_m[simStartDi:])
alphaOp.alphaStatSummary(final_long_alpha[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# * long only strategy will naturally has higher beta compared to long/short
# * meaning it is more correlated to the whole market compared to long/short
# * its IR/ Sharpe ratio is typically lower than long/short, drawdown will be much higher (more volatilie)
# * but return will generally be higher as well, as market generally go up over the long term

# #### momentum strategy

# +
# calculate last 250 days average return, excluding last 20 days due to 20 day reversion effect
# in general stocks exhibit short term reversion but long term momentum

alpha1 = tsOp.movingSum(tsOp.delay(return_m, 20), 230) 
# -

# rank 
alpha1 = alphaOp.groupRank(alpha1)

alpha1 = alphaOp.groupNeutralize(alpha1)

# * assume we can long/short 

# +
final_alpha1 = alpha1.copy()
universe_m = np.ones(final_alpha1.shape).astype(bool)

# scale to 20 mil book
final_alpha1 = alphaOp.scaleAll( final_alpha1, 20e6, universe_m = universe_m, nanIsZero= True )

# +
# test from 2009 as data coverage was too low before that (look at concent50 and 90)
simStartDi = np.where(dates >= 20090101)[0][0]

# delay the alpha position 1 day, because we get end of day information at the end of today trading, 
# but we can only trade/ build position tomorrow and realize the return after tomorrow

final_alpha1[1:] = final_alpha1[:-1]
# -

pnl, tvr, _, _ = simulate.fastSim(final_alpha1[simStartDi:], return_m = return_m[simStartDi:])
alphaOp.alphaStatSummary(final_alpha1[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# if we build all position in the open auction instead
pnl, tvr, _, _ = simulate.fastSim(final_alpha1[simStartDi:], return_m = o2o_return_m[simStartDi:])
alphaOp.alphaStatSummary(final_alpha1[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# * if we long only (get rid of the negative position)

# +
final_long_alpha1 = alpha1.copy()
final_long_alpha1[final_long_alpha1 < 0] = 0
universe_m = np.ones(final_long_alpha1.shape).astype(bool)

# scale to 20 mil book
final_long_alpha1 = alphaOp.scaleAll( final_long_alpha1, 20e6, universe_m = universe_m, nanIsZero= True )

# +
# test from 2009 as data coverage was too low before that (look at concent50 and 90)
simStartDi = np.where(dates >= 20090101)[0][0]

# delay the alpha position 1 day, because we get end of day information at the end of today trading, 
# but we can only trade/ build position tomorrow and realize the return after tomorrow

final_long_alpha1[1:] = final_long_alpha1[:-1]
# -

pnl, tvr, _, _ = simulate.fastSim(final_long_alpha1[simStartDi:], return_m = return_m[simStartDi:])
alphaOp.alphaStatSummary(final_long_alpha1[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# if we build all position in the open auction instead
pnl, tvr, _, _ = simulate.fastSim(final_long_alpha1[simStartDi:], return_m = o2o_return_m[simStartDi:])
alphaOp.alphaStatSummary(final_long_alpha1[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# * momentum is a longer term strategy compared to reversion
# * that's why it has way lower turnover (<10%) compared to reversion turnover (> 70%)
# * since it has low turnover, using close-to-close return or open-to-open return did not make much difference to the result
# because it is not very sensitive to small daily changes (unlike reversion)
#
# * a possible idea could be combining this 2 signals together to diversify and achieve mid level turnover level

# +
# combining reversion and momentum alpha: just simple equal weight 2 portfolio
combined_alpha = final_alpha + final_alpha1

# scale to 20 mil book
combined_alpha = alphaOp.scaleAll( combined_alpha, 20e6, universe_m = universe_m, nanIsZero= True )
# -

pnl, tvr, _, _ = simulate.fastSim(combined_alpha[simStartDi:], return_m = return_m[simStartDi:])
alphaOp.alphaStatSummary(combined_alpha[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

pnl, tvr, _, _ = simulate.fastSim(combined_alpha[simStartDi:], return_m = o2o_return_m[simStartDi:])
alphaOp.alphaStatSummary(combined_alpha[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# * if we consider cost in, lowering turnover without sacrificing too much IR/ return will really help!

# +
# combining reversion and momentum alpha: just simple equal weight 2 portfolio
combined_long_alpha = final_long_alpha + final_long_alpha1

# scale to 20 mil book
combined_long_alpha = alphaOp.scaleAll( combined_long_alpha, 20e6, universe_m = universe_m, nanIsZero= True )
# -

pnl, tvr, _, _ = simulate.fastSim(combined_long_alpha[simStartDi:], return_m = return_m[simStartDi:])
alphaOp.alphaStatSummary(combined_long_alpha[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

pnl, tvr, _, _ = simulate.fastSim(combined_long_alpha[simStartDi:], return_m = o2o_return_m[simStartDi:])
alphaOp.alphaStatSummary(combined_long_alpha[simStartDi:],pnl,tvr,dates[simStartDi:], bookSize=20e6)

# * more ideas ?? 
#  * technical analysis
#  * using order book data (best bid/ best ask)
#  * using volume to detect
#  * trade faster? using intraday data to incorprate in alpha (but where to get data??)
#  
# * more to consider:
#  * T+3 and cost
#  * liquidity of each stock (probably cannot trade all 800 stocks like this, need to choose top liquidity)
#  * minimum lot per stock to trade
#  * volume constraint: we cannot trade, for example, 50% of the daily volume of a stock, it will lead to impact (getting bad
#  price, revealing intention, etc), so we should constraint it to 5-10% of daily volume (approx by last 20 days average)

# ### to be continued......


