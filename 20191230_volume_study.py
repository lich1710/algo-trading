# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

# +
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import alphaOp
import tsOp
import nanOp
import simulate
import cPickle as pickle
# -

# ### Read file

### checking data
filename1 = 'CafeF.RAW_HSX.Upto01.11.2019.csv'
filename2 = 'CafeF.RAW_HNX.Upto01.11.2019.csv'

df1 = pd.read_csv(filename1)
df1['Exchange'] = 'HSX'
df2 = pd.read_csv(filename2)
df2['Exchange'] = 'HNX'

df = df1.append(df2)

# Length of the data
len(df)

df = df.rename(columns={"<Ticker>": "Ticker", "<DTYYYYMMDD>": "Date", "<Open>": "Open","<High>": "High","<Low>": "Low","<Close>": "Close", "<Volume>": "Volume"})

# Get unique tickers
tickers = np.unique(df.Ticker.values).astype(str)
# filter for stock ticker only
stock_tickers = []
for ticker in tickers:
    if len(ticker) == 3:
        stock_tickers.append(ticker)
stock_tickers = np.array(stock_tickers)
print 'Number of stock ticker is: ', len(stock_tickers)

# Get stock df
stock_df = df[df.Ticker.isin(stock_tickers)]

d = {'HSX': 1, 'HNX': 2}
stock_df['Exchange_ID'] = stock_df['Exchange'].map(d)

stock_df.head()

exchange_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Exchange_ID')

exchange_m  = exchange_df.values

close_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Close')
open_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Open')
volume_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Volume')

close_m = close_df.values
open_m  = open_df.values
volume_m  = volume_df.values

close_df.tail()

volume_df.tail()

adv20

dates

# ### investigating volume data

# dollar volume 
dv_m = close_m * volume_m

# time series 20 days moving median
adv20 = tsOp.movingMedian(dv_m, 20)

adv20_rank = alphaOp.groupRank(adv20)

dates  = np.unique(close_df.index)

for threshold in [0.7, 0.8, 0.9]:
    print threshold
    x = adv20.copy()
    x[adv20_rank <= threshold] = np.nan
    
    # number of stocks
    plt.plot(np.sum(np.isfinite(x), axis=1))
    plt.show()
    
    # minimum dollar volume within the percentile
    plt.plot(np.nanmin(x, axis=1))
    plt.show()

# * comment: top 10 percentile in adv20 (~80 stocks) have minimum adv20 of ~ 10-20 bil VND

x = adv20[-1]
x = x[np.isfinite(x)]
plt.hist(x, log=True, bins=100)
plt.show()

print np.sum(x > 5e6)
print np.sum(x > 1e7)
print np.sum(x > 1e8)

# ### investigate 5% of ADV (which is our threshold to trade)

trade_share_m = 0.05 * volume_m

trade_share_m

trade_min_m = np.ones(trade_share_m.shape) * 10
trade_min_m[exchange_m == 2] = 100

print np.sum(trade_min_m == 10, axis=1)
print np.sum(trade_min_m == 100, axis=1)

plt.plot(np.sum(trade_share_m >= trade_min_m, axis=1))
plt.show()

x = adv20.copy()
x[adv20_rank <= .9] = np.nan

plt.plot(np.sum(np.bitwise_and(np.isfinite(x), trade_share_m < trade_min_m), axis=1))
plt.show()

# * comment: the instances of which stocks go into our universe (due to prev 20 days DV) but do not have enough volume today to trade are very few --> can be ignored for now

# ### testing universe op

import utils
reload(utils)

univ_m = np.ones(close_m.shape, dtype=bool)

sub_univ_m = utils.subUniverse(univ_m, stat_m = adv20, univ_size = 100, in_days = 20, out_days = 20)

plt.plot(np.sum(sub_univ_m, axis=1))
plt.show()


