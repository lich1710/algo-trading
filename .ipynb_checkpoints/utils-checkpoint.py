import numpy as np
import matplotlib.pyplot as plt
import warnings
import pandas as pd
import scipy as sp
import scipy.stats
from numba import jit
import numba
import tsOp
import os
import copy
import hashlib

def subUniverse(univ_m, stat_m, univ_size, in_days, out_days):
    sub_univ_m = np.zeros(univ_m.shape, dtype=bool)
    sticky_v = np.zeros(univ_m.shape[1], dtype=int)
    for di in xrange(univ_m.shape[0]):
        daily_univ = univ_m[di]
        daily_stat = stat_m[di]
        daily_stat[~daily_univ] = 0.0
        daily_stat[~np.isfinite(daily_stat)] = 0.0
        temp_rank = daily_stat.argsort()
        temp_rank = np.arange(len(temp_rank))[temp_rank.argsort()]
        temp_rank = max(temp_rank) - temp_rank
        # if start_rank:
        #     temp_idx = (temp_rank <= start_rank + univ_size) & (temp_rank >= start_rank)
        # else:
        temp_idx_included = temp_rank <= univ_size
        temp_idx_excluded = temp_rank > univ_size

        if di > 0:
            sticky_v[temp_idx_included] = np.maximum(1, sticky_v[temp_idx_included] + 1)
            sticky_v[temp_idx_excluded] = np.minimum(-1, sticky_v[temp_idx_excluded] - 1)
            sticky_v = np.maximum(-out_days, np.minimum(60, sticky_v))
            maintain_idx = (sticky_v > -out_days) & (sticky_v < in_days)
            sub_univ_m[di, maintain_idx] = sub_univ_m[di - 1, maintain_idx]
            include_idx = (sticky_v >= in_days)
            sub_univ_m[di, include_idx] = True
            exclude_idx = (sticky_v <= -out_days)
            sub_univ_m[di, exclude_idx] = False
        else:
            sub_univ_m[di] = temp_idx_included
            sub_univ_m[di, sticky_v < 0] = False
        
    return sub_univ_m


