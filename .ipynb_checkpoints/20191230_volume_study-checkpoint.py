# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

# +
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import alphaOp
import tsOp
import nanOp
import simulate
import cPickle as pickle
# -

# ==================================
# # Read and check data

# ### Read file

### checking data
filename1 = 'CafeF.RAW_HSX.Upto01.11.2019.csv'
filename2 = 'CafeF.RAW_HNX.Upto01.11.2019.csv'

df1 = pd.read_csv(filename1)
df1['Exchange'] = 'HSX'
df2 = pd.read_csv(filename2)
df2['Exchange'] = 'HNX'

df = df1.append(df2)

# Length of the data
len(df)

df = df.rename(columns={"<Ticker>": "Ticker", "<DTYYYYMMDD>": "Date", "<Open>": "Open","<High>": "High","<Low>": "Low","<Close>": "Close", "<Volume>": "Volume"})

# Get unique tickers
tickers = np.unique(df.Ticker.values).astype(str)
# filter for stock ticker only
stock_tickers = []
for ticker in tickers:
    if len(ticker) == 3:
        stock_tickers.append(ticker)
stock_tickers = np.array(stock_tickers)
print 'Number of stock ticker is: ', len(stock_tickers)

# Get stock df
stock_df = df[df.Ticker.isin(stock_tickers)]

close_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Close')
open_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Open')
volume_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Volume')

close_m = close_df.values
open_m  = open_df.values
volume_m  = volume_df.values

# ### investigating volume data

dv_m = close_m * volume_m

adv20 = tsOp.movingAverage(dv_m, 20)






