import numpy as np

def fastSim(alphas, return_m=None, bookSize=20e6, buying_fee = 0.2/ 100, selling_fee = 0.35/ 100, close_m=None, vwap_m=None, sbc_m = None, slippage_m=None, isTvr=True, isIndividual=False):
    
    if close_m is None or vwap_m is None:
        closeToVwapRet_m = None
    else:
        closeToVwapRet_m = close_m / vwap_m - 1.0
        closeToVwapRet_m[~np.isfinite(closeToVwapRet_m)] = 0.0

    numdates = alphas.shape[0]

    isPosChange = isTvr or (closeToVwapRet_m is not None) or (slippage_m is not None)
    if isPosChange:
        alphaDiff = np.diff(alphas, axis=0)
    else:
        alphaDiff = None

    pnl_m = np.zeros(alphas.shape, dtype=np.float32)
    if closeToVwapRet_m is None:
        pnl_m[1:, :] = alphas[:-1, :] * return_m[-numdates + 1:, :]
    else:
        pnl_m[1:, :] = alphas[:-1, :] * return_m[-numdates + 1:, :] + alphaDiff * closeToVwapRet_m[-numdates + 1:, :]
        
    if isTvr:
        tvr_m = np.zeros(alphas.shape, dtype=np.float32)
        if bookSize is None:
            #bookSize = np.median(np.sum(np.fabs(alphas), axis=1), axis=1)
            bookSize = np.sum(np.fabs(alphas), axis = 1)
            tvr_m[1:, :] = (np.fabs(alphaDiff).T / bookSize[:-1]).T
        # else:
        #     bookSize *= 2.0
        tvr_m[1:, :] = np.fabs(alphaDiff) / bookSize
    else:
        tvr_m = 0.0
    
    buy_m = alphaDiff.copy()
    buy_m[buy_m < 0] = 0
    sell_m = alphaDiff.copy()
    sell_m[sell_m > 0] = 0
    
    pnl_m[1:, :] -= buy_m * buying_fee
    pnl_m[1:, :] -= np.fabs(sell_m) * selling_fee
    
    if not (sbc_m is None):
        cost_m = alphas * sbc_m
        cost_m[cost_m > 0.] = 0.
        cost_m[~np.isfinite(cost_m)] = 0.
        pnl_m[1:] = pnl_m[1:] + cost_m[:-1]
    
    if not (slippage_m is None):
        pnl_m[1:] -= slippage_m * np.fabs(alphaDiff)
    
    pnl_m[~np.isfinite(pnl_m)] = 0.0
      
    if not isIndividual:
        #pnl_m[~np.isfinite(pnl_m)] = 0.0
        pnl_m = np.sum(pnl_m, axis=1)
        if isTvr:
            tvr_m[~np.isfinite(tvr_m)] = 0.0
            tvr_m = np.sum(tvr_m, axis=1)
    
    return (pnl_m, tvr_m)

def fastSim_(alphas, return_m=None, bookSize=20e6, close_m=None, vwap_m=None, sbc_m = None, slippage_m=None, isTvr=True,
            check_long_short_side_pnl = True, isIndividual=False):
    # MK# ???
    if close_m is None or vwap_m is None:
        closeToVwapRet_m = None
    else:
        closeToVwapRet_m = close_m / vwap_m - 1.0
        closeToVwapRet_m[~np.isfinite(closeToVwapRet_m)] = 0.0

    numdates = alphas.shape[0]

    isPosChange = isTvr or (closeToVwapRet_m is not None) or (slippage_m is not None)
    if isPosChange:
        alphaDiff = np.diff(alphas, axis=0)
    else:
        alphaDiff = None

    pnl_m = np.zeros(alphas.shape, dtype=np.float32)
    if closeToVwapRet_m is None:
        pnl_m[1:, :] = alphas[:-1, :] * return_m[-numdates + 1:, :]
    else:
        pnl_m[1:, :] = alphas[:-1, :] * return_m[-numdates + 1:, :] + alphaDiff * closeToVwapRet_m[-numdates + 1:, :]

    if isTvr:
        tvr_m = np.zeros(alphas.shape, dtype=np.float32)
        if bookSize is None:
            #bookSize = np.median(np.sum(np.fabs(alphas), axis=1), axis=1)
            bookSize = np.sum(np.fabs(alphas), axis = 1)
            tvr_m[1:, :] = (np.fabs(alphaDiff).T / bookSize[:-1]).T
        # else:
        #     bookSize *= 2.0
        tvr_m[1:, :] = np.fabs(alphaDiff) / bookSize
    else:
        tvr_m = 0.0
    
    if not (sbc_m is None):
        cost_m = alphas * sbc_m
        cost_m[cost_m > 0.] = 0.
        cost_m[~np.isfinite(cost_m)] = 0.
        pnl_m[1:] = pnl_m[1:] + cost_m[:-1]
    
    if not (slippage_m is None):
        pnl_m[1:] -= slippage_m * np.fabs(alphaDiff)
    
    pnl_m[~np.isfinite(pnl_m)] = 0.0
    
    if check_long_short_side_pnl:
        alpha_delay = np.copy(alphas)
        alpha_delay[1:] = alphas[:-1] 
        pnl_long_v = np.sum(pnl_m * (alpha_delay > 0), axis=1)
        pnl_short_v = np.sum(pnl_m * (alpha_delay < 0), axis=1)
    else:
        pnl_long_v = None
        pnl_short_v = None
    
    if not isIndividual:
        #pnl_m[~np.isfinite(pnl_m)] = 0.0
        pnl_m = np.sum(pnl_m, axis=1)
        if isTvr:
            tvr_m[~np.isfinite(tvr_m)] = 0.0
            tvr_m = np.sum(tvr_m, axis=1)
    
    return (pnl_m, tvr_m, pnl_long_v, pnl_short_v)

def di_simulate(alpha, building_price, liquidating_price, building_spread = None, building_slippage = 0.5, liquidating_spread = None, liquidating_slippage = 0.5, fee_m = None, book_size=None):
    numdates = alpha.shape[0]
    pnl_m = np.zeros(alpha.shape, dtype=np.float32)
    
    price_return = liquidating_price/ building_price - 1
    
    pnl_m = alpha * price_return
    
    
    if fee_m is not None:

        cost_m = alpha * fee_m
        cost_m[cost_m > 0.] = 0.
        # cost_m[-1] = 0.
        cost_m[~np.isfinite(cost_m)] = 0.

        # pnl_v += np.nansum(cost_m, axis=1)
        pnl_m = pnl_m + cost_m

    if building_spread is not None:
        building_cost_m = np.zeros_like(alpha, dtype=float)
        building_cost_m = np.fabs(alpha) * building_slippage * building_spread
        building_cost_m[~np.isfinite(building_cost_m)] = 0.

        pnl_m = pnl_m - np.fabs(building_cost_m)
    
    if liquidating_spread is not None:
        liquidating_cost_m = np.zeros_like(alpha, dtype=float)
        liquidating_cost_m = np.fabs(alpha) * liquidating_slippage * liquidating_spread
        liquidating_cost_m[~np.isfinite(liquidating_cost_m)] = 0.

        pnl_m = pnl_m - np.fabs(liquidating_cost_m)
        
    pnl = np.nansum((pnl_m), axis=1)
    
    if book_size is None:
        book_size = np.sum(np.fabs(alpha), axis=1)
    
    alphaSum = np.nansum(np.fabs(alpha), axis=1)
    tvr = np.zeros(alphaSum.shape, dtype=np.float32)
    tvr = (alphaSum / book_size)*2

    return (pnl, tvr)

def compute_IR(pnl_v):
    return np.mean(pnl_v) / np.std(pnl_v)

def compute_pnl(tpm_m,
                return_m,
                open_to_close = False,
                close_to_vwap_m = None,
                fee_m=None, 
                spread_m=None,
                slippage_constant=0.5,
                stock_level=False):

    pnl_m = np.zeros(tpm_m.shape)
    
    # holding pnl
    if open_to_close is False:
        pnl_m[1 : ] = tpm_m[: -1, :] * return_m[1 : , :]
    else:
        pnl_m[1 : ] = tpm_m[: -1, :] * return_m[: -1, :]

    # trading pnl assuming filled at vwap
    if close_to_vwap_m is not None:
        pnl_m[1:] += (close_to_vwap_m[1:] * np.diff(tpm_m, axis=0))
    
    if fee_m is not None:

        cost_m = tpm_m * fee_m
        cost_m[cost_m > 0.] = 0.
        # cost_m[-1] = 0.
        cost_m[~np.isfinite(cost_m)] = 0.

        # pnl_v += np.nansum(cost_m, axis=1)
        pnl_m[1:] = pnl_m[1:] + cost_m[:-1]

    if spread_m is not None:
        cost_m = np.zeros_like(tpm_m, dtype=float)
        cost_m[1 : ] = np.diff(tpm_m, axis=0) * slippage_constant * spread_m[1 : ]
        cost_m[~np.isfinite(cost_m)] = 0.

        pnl_m = pnl_m - np.fabs(cost_m)

    pnl_v = np.nansum(pnl_m, axis=1)
    #pnl_v = np.nansum(np.fabs(tpm_m), axis=1)
    #pnl_v[~np.isfinite(pnl_v)] = 0.

    if stock_level:
        return pnl_m
    else:
        return pnl_v

def compute_turnover(tpm_m):
    """
    NaN should be zerorized here.
    """
    tpm_m = tpm_m.copy()
    tpm_m[~np.isfinite(tpm_m)] = 0.
    tvr_m = np.full_like(tpm_m, fill_value=np.nan, dtype=float)

    tvr_m[1 : , :] = np.diff(tpm_m, axis=0)
    tvr_v = np.nansum(np.fabs(tvr_m), axis=1) / np.nansum(np.fabs(tpm_m), axis=1)
    tvr_v[~np.isfinite(tvr_v)] = np.nan

    return tvr_v

def compute_MDD(ret_v):
    cum_pnl_v = ret_v.cumsum()
    cumsum_max = -np.inf
    MDD = -np.inf

    for k, curr in enumerate(cum_pnl_v):
        cumsum_max = max(cumsum_max, curr)
        DD = cumsum_max - curr
        MDD = max(MDD, DD)

    return MDD

def compute_hit_rate(pnl_v):
    hit_v = np.sign(pnl_v)
    hit_v += 1
    hit_v /= 2.

    return hit_v

def compute_concentration(tpm_m, percentile=0.5):
    tpm_m = np.fabs(tpm_m)
    tpm_m[~np.isfinite(tpm_m)] = 0.
    tpm_m = -np.sort(-np.fabs(tpm_m), axis=1)  # Sort in descending order
    cumsum_m = np.cumsum(tpm_m, axis=1)

    booksize_v = cumsum_m[:, -1]
    mask_m = cumsum_m < booksize_v[:, np.newaxis] * percentile
    concent_v = mask_m.sum(axis=1)

    return concent_v

def pnl_graph(ret_v,
              x_tick_pos,
              x_tick_label,
              loc=None):

    plt.figure()

    ax = plt.subplot(111)
    ax.plot(ret_v)
    ax.set_xlabel('Time')
    ax.set_ylabel('Cumulative Return')
    if loc is not None:
        for i in range(len(loc)):
             ax.axvline(loc[i], c='red')
    ax.xaxis.grid(True, which='major')
    ax.set_xlim([0, ret_v.shape[0]])

    plt.xticks(x_tick_pos, x_tick_label)
    plt.show()

def simulate(tpm_m,
             return_m,
             dates,
             open_to_close = False,
             close_to_vwap_m = None,
             pnl_v=None,
             fee_m=None,
             pnl_t_m=None,
             spread_m=None,
             slippage_constant=0.5,
             plot=True,
             universe_m=None,
             loc=None):
    """
    Provides annual and aggregate stats for:
      * IR (needs to be iteratively calculated)
      * Return
      * Turnover
      * MDD (needs to be iteratively calculated)
      * Hit rate
      * Concentration 50%
      * Concentration 90%

    """
    years = np.unique(dates / 10000)

    if universe_m is not None:
        if universe_m.shape != (len(self.date_list()), len(self.trim)):
            raise ValueError('universe_m shape mismatch: %s != %s' % (universe_m.shape, (len(self.date_list()), len(self.trim))))

        tpm_m = tpm_m.copy()
        tpm_m[~universe_m] = 0.

    if pnl_v is None:
        pnl_v = compute_pnl(tpm_m, return_m, open_to_close = open_to_close, close_to_vwap_m = close_to_vwap_m, fee_m = fee_m, spread_m = spread_m, slippage_constant=slippage_constant) 
    ret_v = pnl_v/ np.nansum(np.fabs(tpm_m), axis=1)
    ret_v[~np.isfinite(ret_v)] = 0.

    tvr_v = compute_turnover(tpm_m)
    hit_rate_v = compute_hit_rate(pnl_v)
    concent50_v = compute_concentration(tpm_m, 0.5)
    concent90_v = compute_concentration(tpm_m, 0.9)

    x_tick_pos = np.zeros(years.shape[0])

    for yi, year in enumerate(years):
        x_tick_pos[yi] = np.where(dates == dates[dates > year * 10000][0])[0]

    if plot:
        pnl_graph(pnl_v.cumsum(), x_tick_pos, years, loc)

        summary = np.zeros([years.shape[0] + 1, 7])

        # Compute annual statistics
        for yi, year in enumerate(years):
            di_v = np.argwhere(dates / 10000 == year)

            ir = compute_IR(ret_v[di_v])
            cum_pnl = ret_v[di_v].cumsum()[-1] * 100 * 252. / len(di_v)
            mean_tvr = np.nanmean(tvr_v[di_v]) * 100
            mdd = compute_MDD(ret_v[di_v]) * 100
            hit_rate = np.nanmean(hit_rate_v[di_v]) * 100
            concent50 = np.nanmedian(concent50_v[di_v])
            concent90 = np.nanmedian(concent90_v[di_v])

            summary[yi, :] = [
                ir,
                cum_pnl,
                mean_tvr,
                mdd,
                hit_rate,
                concent50,
                concent90
            ]

        # Compute aggregate statistics
        ir = compute_IR(ret_v)
        cum_pnl = ret_v.cumsum()[-1] * 100 / (dates.shape[0] / 252.)
        mean_tvr = np.nanmean(tvr_v) * 100
        mdd = compute_MDD(ret_v) * 100
        hit_rate = np.nanmean(hit_rate_v) * 100
        concent50 = np.nanmedian(concent50_v)
        concent90 = np.nanmedian(concent90_v)

        summary[-1, :] = [
            ir,
            cum_pnl,
            mean_tvr,
            mdd,
            hit_rate,
            concent50,
            concent90
        ]

        pd.set_option('display.float_format', '{:,.2f}'.format)

        summary_table = pd.DataFrame(
            summary,
            index=np.append(years, 'Total'),
            columns=[
                'IR',
                'Annual Return (%)',
                'Turnover (%)',
                'MDD (%)',
                'Hit Rate (%)',
                'Concent50',
                'Concent90'
            ]
        )

        summary_table['IR'] = summary_table['IR'].map('{:,.3f}'.format)
        summary_table['Concent50'] = summary_table['Concent50'].map('{:,.0f}'.format)
        summary_table['Concent90'] = summary_table['Concent90'].map('{:,.0f}'.format)

        try:
            from IPython.core.display import display

            display(summary_table)

        except ImportError:
            print summary_table

        pd.reset_option('display.float_format')

    return pnl_v, tvr_v







