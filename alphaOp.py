import numpy as np
import matplotlib.pyplot as plt
import warnings
import pandas as pd
import scipy as sp
import scipy.stats
from numba import jit
import numba
import tsOp
import os
import copy
import hashlib

def groupWeightedMean( alpha_m, universe_m, weight_m, group_m, groupDelay = 0):
     # calculate weighted cross-sectional mean in each group
    
    alpha = np.copy(alpha_m)
    valid = np.bitwise_and( universe_m, np.isfinite( alpha  ) )
    result = np.zeros(alpha_m.shape) * np.nan
    
    groupValid_m = np.copy(group_m)
    groupValid_m[ ~valid ] = -1
    
    for di, dailyAlpha in enumerate( alpha ):
        dailyWeight = np.copy(weight_m[di])
        dailyUniv = np.copy(universe_m[di])
        dailyWeight[~np.isfinite(dailyAlpha)] = np.nan
        dailyWeight[~dailyUniv] = np.nan
        dailyAlpha[~np.isfinite(dailyWeight)] = np.nan
        dailyAlpha[~dailyUniv] = np.nan
       
        dailyGroup = group_m[di - groupDelay, :]
        
        uniqueGroup_v = np.unique(dailyGroup)
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[uniqueGroup_v >= 0]
        
        for gi, groupIx in enumerate(uniqueGroup_v):
            groupPick = (dailyGroup == groupIx)
            dailyAlpha[groupPick] = np.nansum( dailyAlpha[groupPick]*dailyWeight[groupPick] )/np.nansum(dailyWeight[groupPick])
                
        result[di] = dailyAlpha

    return result

def groupZscore( alpha_m, universe_m = None, group_m = None, nminGroupSize = 4, mergeSmallGroups = False, delay = 0, excludeZero = False ):

    result = np.copy( alpha_m )
    if universe_m is None:
        valid = np.isfinite( result )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( result ) ) # exclude data not in the universe and nan data in alpha
    
    if excludeZero == True:
        valid = np.bitwise_and( valid, result != 0.0 ) # exclude zero data as no signal

    if group_m is None:
        print 'there is no group'
        group_m = valid.astype('int32')

    groupValid_m = np.copy(group_m)
    groupValid_m[ ~valid ] = -1

    for di, dailyAlpha in enumerate( result ):
        dailyUniv = valid[di, :]
        groupDelay = delay if di > 0 else 0
        dailyGroup = groupValid_m[di - groupDelay, :]
        
        uniqueGroup_v = np.unique(dailyGroup)
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[uniqueGroup_v >= 0]

        if mergeSmallGroups:
            smallGroupIx_v = np.copy(dailyUniv)
            smallGroupIx_v[:] = False

        for gi, groupIx in enumerate(uniqueGroup_v):
            groupPick = np.bitwise_and(dailyUniv, dailyGroup == groupIx)
            if mergeSmallGroups:
                if np.sum(groupPick) >= minGroupSize:
                    dailyAlpha[groupPick] = scipy.stats.zscore(dailyAlpha[groupPick])
                else:
                    smallGroupIx_v[groupPick] = True
            else: 
                dailyAlpha[groupPick] = scipy.stats.zscore(dailyAlpha[groupPick])
            
        if mergeSmallGroups and np.count_nonzero(smallGroupIx_v) > 0:
            dailyAlpha[smallGroupIx_v] = scipy.stats.zscore(dailyAlpha[smallGroupIx_v])
            
        result[di] = dailyAlpha
    
    result[~valid] = np.nan
    result[~np.isfinite(group_m)] = np.nan
        
    return result
        
def vectorRank( input_v, normalize = True, neutralize = False, separate = False ):
    # daily rank
    # use sp.stats.rankdata to rank a row vector
    
    result = np.copy( input_v )
    if separate:
        tempIx = result > 0
        result[ tempIx ] = vectorRank( result[ tempIx ], normalize = False, separate = False )
        result[ ~tempIx ] = vectorRank( result[ ~tempIx ], normalize = False, separate = False)
        result[~tempIx] += np.amax( result[ tempIx ] )
    else:
        #result = sp.stats.mstats.rankdata( np.ma.masked_invalid(result))
        result = sp.stats.rankdata( result, method = 'average' )
        
    if normalize:
        result /= np.amax( result )     
    if neutralize:
        result[result>0] -= np.mean( result[result>0] )    
    return result

def groupRank( alpha_m, universe_m = None, group_m = None, normalize = True, neutralize = False, separate = False, minGroupSize = 4, mergeSmallGroups = False, delay = 0, excludeZero = False ):
    # rank within each group (for example group by industry)
    # if some group has less than minGroupSize, merge them together and rank

    result = np.copy( alpha_m )
    if universe_m is None:
        valid = np.isfinite( result )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( result ) ) # exclude data not in the universe and nan data in alpha
    
    if excludeZero == True:
        valid = np.bitwise_and( valid, result != 0.0 ) # exclude zero data as no signal

    if group_m is None:
        print 'there is no group'
        group_m = valid.astype('int32')

    groupValid_m = np.copy(group_m)
    groupValid_m[ ~valid ] = -1

    for di, dailyAlpha in enumerate( result ):
        dailyUniv = valid[di, :]
        groupDelay = delay if di > 0 else 0
        dailyGroup = groupValid_m[di - groupDelay, :]
        
        uniqueGroup_v = np.unique(dailyGroup)
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[uniqueGroup_v >= 0]

        if mergeSmallGroups:
            smallGroupIx_v = np.copy(dailyUniv)
            smallGroupIx_v[:] = False

        for gi, groupIx in enumerate(uniqueGroup_v):
            groupPick = np.bitwise_and(dailyUniv, dailyGroup == groupIx)
            if mergeSmallGroups:
                if np.sum(groupPick) >= minGroupSize:
                    dailyAlpha[groupPick] = vectorRank( dailyAlpha[groupPick], normalize = normalize, neutralize = neutralize, separate = separate )
                else:
                    smallGroupIx_v[groupPick] = True
            else: 
                dailyAlpha[groupPick] = vectorRank( dailyAlpha[groupPick], normalize = normalize, neutralize = neutralize, separate = separate )
            
        if mergeSmallGroups and np.count_nonzero(smallGroupIx_v) > 0:
            dailyAlpha[smallGroupIx_v] = vectorRank( dailyAlpha[smallGroupIx_v], normalize = normalize, neutralize = neutralize, separate = separate )
            
        result[di] = dailyAlpha
    
    result[~valid] = np.nan
    result[~np.isfinite(group_m)] = np.nan
        
    return result

def winsorize(input_m,limit=0.05):
    input_n = np.copy(input_m)
    for i in range(len(input_n)):
        input_n[i,:] = scipy.stats.mstats.winsorize(input_n[i,:], limit).data
    return input_n

def groupWinsorize( alpha_m, limit = 0.05, universe_m = None, group_m = None, separate = False, minGroupSize = 4, mergeSmallGroups = False, delay = 0, excludeZero = False ):
    # rank within each group (for example group by industry)
    # if some group has less than minGroupSize, merge them together and rank

    result = np.copy( alpha_m )
    if universe_m is None:
        valid = np.isfinite( result )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( result ) ) # exclude data not in the universe and nan data in alpha
    
    if excludeZero == True:
        valid = np.bitwise_and( valid, result != 0.0 ) # exclude zero data as no signal

    if group_m is None:
        print 'there is no group'
        group_m = valid.astype('int32')

    groupValid_m = np.copy(group_m)
    groupValid_m[ ~valid ] = -1

    for di, dailyAlpha in enumerate( result ):
        dailyUniv = valid[di, :]
        groupDelay = delay if di > 0 else 0
        dailyGroup = groupValid_m[di - groupDelay, :]
        
        uniqueGroup_v = np.unique(dailyGroup)
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[uniqueGroup_v >= 0]

        if mergeSmallGroups:
            smallGroupIx_v = np.copy(dailyUniv)
            smallGroupIx_v[:] = False

        for gi, groupIx in enumerate(uniqueGroup_v):
            groupPick = np.bitwise_and(dailyUniv, dailyGroup == groupIx)
            if mergeSmallGroups:
                if np.sum(groupPick) >= minGroupSize:
                    dailyAlpha[groupPick] = scipy.stats.mstats.winsorize( dailyAlpha[groupPick], limits = limit )
                else:
                    smallGroupIx_v[groupPick] = True
            else: 
                dailyAlpha[groupPick] = scipy.stats.mstats.winsorize( dailyAlpha[groupPick], limits = limit )
            
        if mergeSmallGroups and np.count_nonzero(smallGroupIx_v) > 0:
            dailyAlpha[smallGroupIx_v] = scipy.stats.mstats.winsorize( dailyAlpha[smallGroupIx_v], limits = limit )
            
        result[di] = dailyAlpha
    
    result[~valid] = np.nan
    result[~np.isfinite(group_m)] = np.nan
        
    return result


def groupNeutralize( alpha_m, universe_m = None, group_m = None, minGroupSize = 2, mergeSmallGroups = False, excludeSmall = True, delay = 0 , excludeZero = False):
    neutAlpha_m = np.copy( alpha_m )

    if universe_m is None:
        valid = np.isfinite( neutAlpha_m )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( neutAlpha_m ) )

    if excludeZero == True:
        valid = np.bitwise_and( valid, neutAlpha_m != 0 ) # no signal
    
    if group_m is None:
        print 'there is no group'
        group_m = valid.astype('int32')
    
    groupValid_m = np.copy(group_m)
    groupValid_m[ ~valid ] = -1
    
    for di, dailyAlpha in enumerate( neutAlpha_m ):
        # dailyAlpha = np.copy( tempAlpha )
        dailyUniv = valid[ di, : ]
        groupDelay = delay if di > 0 else 0
        dailyGroup = group_m[ di - groupDelay, : ]
        
        uniqueGroup_v = np.unique( dailyGroup )
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[ uniqueGroup_v >= 0 ]

        smallGroupIx_v = np.copy( dailyUniv )
        smallGroupIx_v[:] = False

        for gi, groupIx in enumerate( uniqueGroup_v ):
            groupPick = np.bitwise_and( dailyUniv, dailyGroup == groupIx )
            if np.sum(groupPick) >= minGroupSize:
                dailyAlpha[ groupPick ] -= np.nanmean( dailyAlpha[ groupPick ] )
            else:
                if mergeSmallGroups is False and excludeSmall is False:
                    dailyAlpha[ groupPick ] -= np.nanmean( dailyAlpha[ groupPick ] )
                else:
                    smallGroupIx_v[ groupPick ] = True
            
        if np.count_nonzero(smallGroupIx_v) > 0:
            if mergeSmallGroups:
                dailyAlpha[ smallGroupIx_v ] -= np.mean( dailyAlpha[ smallGroupIx_v ] )
            else:
                dailyAlpha[ smallGroupIx_v ] = np.nan
        
        dailyAlpha[~dailyUniv] = np.nan
        neutAlpha_m[ di ] = dailyAlpha

    neutAlpha_m[~valid] = np.nan
    neutAlpha_m[~np.isfinite(group_m)] = np.nan
    #neutAlpha_m[~np.isfinite(neutAlpha_m)] = np.nan 
        
    return neutAlpha_m


def scale( alpha_v, bookSize ):
    # daily scaling
    result = np.copy( alpha_v )
    scaleFactor = bookSize / np.nansum( np.fabs( result ) )
    # print scaleFactor
    if np.isfinite( scaleFactor ):
        result *= scaleFactor

    return result
        
def scaleAll( alpha_m, bookSize, universe_m = None, nanIsZero = True, keepNan = False ):
    
    result = np.copy( alpha_m )
    if not ( universe_m is None ):
        assert alpha_m.shape == universe_m.shape
        result[ ~universe_m ] = 0.

    # print 'Original median book size: $%.0f' % np.median( np.sum( np.fabs( alpha_m ), axis=1 ) )
    for di, alpha_v in enumerate( result ):
        validIx = np.isfinite( alpha_v )
        alpha_v[ ~validIx ] = 0.0
        alpha_v = scale( alpha_v, bookSize )
        if not nanIsZero and keepNan:
            if np.sum( ~validIx ) > 0:
                alpha_v[ ~validIx ] = np.nan
        result[ di, : ] = alpha_v
    return result

def scaleGroup( alpha_m, group_m, bookSize, universe_m = None, nanIsZero = False, keepNan = True,  minGroupSize = 4, mergeSmallGroups = False, delay = 0  ):
   
    result = np.copy( alpha_m )
    if not ( universe_m is None ):
        assert alpha_m.shape == universe_m.shape
        result[ ~universe_m ] = 0.0
    

    if group_m is None:
        print 'there is no group'
        group_m = universe_m.astype('int32')

    groupValid_m = np.copy(group_m)
    if not ( universe_m is None ):
        groupValid_m[ ~universe_m ] = -1

    for di, dailyAlpha in enumerate( result ):
        validIx = np.isfinite( dailyAlpha )
        groupDelay = delay if di > 0 else 0
        dailyGroup = groupValid_m[di - groupDelay, :]
        
        uniqueGroup_v = np.unique(dailyGroup)
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[uniqueGroup_v >= 0]

        if mergeSmallGroups:
            smallGroupIx_v = np.copy(validIx)
            smallGroupIx_v[:] = False

        for gi, groupIx in enumerate(uniqueGroup_v):
            groupPick = np.bitwise_and(validIx, dailyGroup == groupIx)
            if mergeSmallGroups:
                if np.sum(groupPick) >= minGroupSize:
                    dailyAlpha[groupPick] = scale ( dailyAlpha[groupPick], bookSize = bookSize)
                else:
                    smallGroupIx_v[groupPick] = True
            else: 
                dailyAlpha[groupPick] = scale ( dailyAlpha[groupPick], bookSize = bookSize )
            
        if mergeSmallGroups and np.count_nonzero(smallGroupIx_v) > 0:
            dailyAlpha[smallGroupIx_v] = scale ( dailyAlpha[smallGroupIx_v], bookSize = bookSize )
        
        if ~nanIsZero and keepNan:
            if np.sum( ~validIx ) > 0:
                dailyAlpha[ ~validIx ] = np.nan
        
        result[di] = dailyAlpha
    
    return result

def maxPositionDaily( input_v, valid_v, maxPositionValue ):
    alpha_v = np.copy( input_v )
    dailyBooksize = np.sum( np.fabs( alpha_v[ valid_v ] ) )
    largePostionIx_v = np.bitwise_and( valid_v, np.fabs( alpha_v ) > maxPositionValue * dailyBooksize )
    if ( 1.0 / maxPositionValue ) >= np.sum( largePostionIx_v ):
        alpha_v[ largePostionIx_v ] = maxPositionValue * dailyBooksize * np.sign( alpha_v[ largePostionIx_v ] )
    else:
        warnings.warn( 'Too small number of instruments to impose maxPosition: %d v. %.3f' % ( np.sum( largePostionIx_v ), maxPositionValue ) )

    return alpha_v

def maxPosition( alpha_m, universe_m = None, maxPositionValue = 0.05, iterNum = 10 ):
    result = np.copy(alpha_m)
    if universe_m is None:
        valid = np.isfinite( result )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( result ) )

    valid = np.bitwise_and( valid, result != 0 )

    for di in xrange( result.shape[0] ):
        dailyBookSize = np.sum( np.fabs( result[ di, valid[ di, : ] ] ) )
        if dailyBookSize > 0.0:
            for iter in xrange( iterNum ):
                result[di] = maxPositionDaily( result[ di ], valid[ di ], maxPositionValue )
        else:
            warnings.warn('dailyBooksize is zero!')
    return result

def remap( alpha_m, universe_m = None, bookSize = 20e6, normalize = False, balance = True, minInstNum = 2):  #normalize -> normalize 
    result = np.copy( alpha_m )
    assert balance | normalize, "Only one of neutralize or balance should be selected"
    # Note that balance guarantees neutralize and neutralize guarantees balance
    # But neutralize can alter long/short signals where balance preserves the original sign
    if universe_m is None:
        valid = np.isfinite( result )
    else:
        valid = np.bitwise_and(universe_m, np.isfinite( result ))

    for di in xrange(result.shape[0]):
        dailyBookSize = np.sum(np.fabs(result[di, valid[di, :]]))
        if dailyBookSize == 0.0:
            warnings.warn('dailyBooksize is zero!')
        elif np.sum( valid ) < minInstNum:
            warnings.warn('too small number of instrument(s) to remap!' )
        else:
            if normalize:
                result[di, valid[di, :]] -= np.mean(result[di, valid[di, :]])
            if balance:
                positiveIx_v = np.bitwise_and( valid[ di, : ], result[di, :] > 0)
                negativeIx_v = np.bitwise_and( valid[ di, : ], result[di, :] < 0)
                if ( np.sum( positiveIx_v ) > 10 * np.sum( negativeIx_v ) ) or ( np.sum( positiveIx_v ) < 0.1 * np.sum( negativeIx_v ) ):
                    warnings.warn('long-short counts are too imbalanced!')
                result[di, negativeIx_v] *= np.sum(np.fabs(result[di, positiveIx_v])) / np.sum(np.fabs(result[di, negativeIx_v]))
            result[di, valid[di, :]] *= bookSize / np.sum(np.fabs(result[di, valid[di, :]]))
    return result

def remap_pandas(alpha, universe, booksize=20e6, neutralize=False, balance=True, scale=True):
    
    alpha = pd.DataFrame(alpha)
    universe = pd.DataFrame(universe)
    
    result = alpha[universe].copy()
    if neutralize:
        result = result.subtract(result.mean(axis=1), axis=0)
    if balance:
        adj_factor = -result[result>0].sum(axis=1)/result[result<0].sum(axis=1)
        result[result<0] = result[result<0].multiply(adj_factor, axis=0)
    if scale:
        result = result.divide(result.abs().sum(axis=1), axis=0)*booksize
    return result.values

def barrierSimple( alpha_m, barrierValue, bookSize = None, universe_m = None, backdays = 1, noiseFactor = 0.0):
    if universe_m is None:
        valid = np.isfinite( alpha_m )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( alpha_m ) )

    result = np.copy( alpha_m )
    # result[ ~valid ] = 0.0
    for di in xrange( result.shape[0] ):
        if di >= backdays:
            alphaDiff_v = result[ di ] - result[ di - 1 ]
            tempValid_v = np.bitwise_or( valid[ di ], valid[ di - 1 ] )
            barrierDollarSize = np.fabs( barrierValue * np.sum( np.fabs( result[ di, tempValid_v ] ) ) if bookSize is None else barrierValue * bookSize )
            buyIx_v = np.bitwise_and( alphaDiff_v >= barrierDollarSize, tempValid_v )
            sellIx_v = np.bitwise_and( alphaDiff_v <= - barrierDollarSize, tempValid_v )
            noiseIx_v = np.bitwise_and( ~np.bitwise_or( buyIx_v, sellIx_v ), tempValid_v )
            # noiseIx_v = np.bitwise_and( ~buyIx_v, ~sellIx_v)
            # noiseIx_v = np.bitwise_and( noiseIx_v, tempValid_v)

            if noiseFactor == 0:
                alphaDiff_v[noiseIx_v] = 0
            else:
                alphaDiff_v[ noiseIx_v ] *= noiseFactor
            alphaDiff_v[ buyIx_v ] -= barrierDollarSize
            alphaDiff_v[ sellIx_v ] += barrierDollarSize
            result[ di ] = result[ di - 1 ] + alphaDiff_v

    return result

def barrierSimple_trade( alpha_m, barrierValue, universe_m = None, backdays = 1, noiseFactor = 0.0):
    if universe_m is None:
        valid = np.isfinite( alpha_m )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( alpha_m ) )

    result = np.copy( alpha_m )
    # result[ ~valid ] = 0.0
    for di in xrange( result.shape[0] ):
        if di >= backdays:
            alphaDiff_v = result[ di ] - result[ di - 1 ]
            trade_size = np.nansum(np.fabs(alphaDiff_v))
            tempValid_v = np.bitwise_or( valid[ di ], valid[ di - 1 ] )
            barrierDollarSize = barrierValue * trade_size
            buyIx_v = np.bitwise_and( alphaDiff_v >= barrierDollarSize, tempValid_v )
            sellIx_v = np.bitwise_and( alphaDiff_v <= - barrierDollarSize, tempValid_v )
            noiseIx_v = np.bitwise_and( ~np.bitwise_or( buyIx_v, sellIx_v ), tempValid_v )
            # noiseIx_v = np.bitwise_and( ~buyIx_v, ~sellIx_v)
            # noiseIx_v = np.bitwise_and( noiseIx_v, tempValid_v)

            if noiseFactor == 0:
                alphaDiff_v[noiseIx_v] = 0
            else:
                alphaDiff_v[ noiseIx_v ] *= noiseFactor
            alphaDiff_v[ buyIx_v ] -= barrierDollarSize
            alphaDiff_v[ sellIx_v ] += barrierDollarSize
            result[ di ] = result[ di - 1 ] + alphaDiff_v

    return result

def barrierRefresh(alpha_m, barrierValue, bookSize=None, universe_m=None, backdays=1, noiseFactor=0., refreshPeriod=3):
    assert refreshPeriod > 0
    assert noiseFactor >= 0
    if universe_m is None:
        valid = np.isfinite(alpha_m)
    else:
        valid = np.logical_and(universe_m, np.isfinite(alpha_m))

    result = alpha_m.copy()
    frustrated_trade = np.zeros((1, result.shape[1]))
    frustrated_trade_previous = frustrated_trade.copy()
    frustrated_count = np.zeros_like(frustrated_trade, dtype=np.int)
    for di in xrange(result.shape[0]):
        if di >= backdays:
            alphaDiff_v = result[di] - result[di - 1]
            tempValid_v = np.logical_or(valid[di], valid[di - 1])
            if bookSize is None:
                barrierDollarSize = np.abs(barrierValue * np.sum(np.fabs(result[di, tempValid_v])))
            else:
                barrierDollarSize = abs(barrierValue * bookSize)
            buyIx_v = np.logical_and(alphaDiff_v >= barrierDollarSize, tempValid_v)
            sellIx_v = np.logical_and(alphaDiff_v <= -barrierDollarSize, tempValid_v)
            noiseIx_v = np.logical_and(~np.logical_or(buyIx_v, sellIx_v), tempValid_v)
            frustrated_trade = noiseIx_v * alphaDiff_v
            alphaDiff_v[noiseIx_v] *= noiseFactor
            alphaDiff_v[buyIx_v] -= barrierDollarSize
            alphaDiff_v[sellIx_v] += barrierDollarSize
            
            # allow persisting small trade
            frustrated_count = (frustrated_trade != 0) * \
                               (np.sign(frustrated_trade) == np.sign(frustrated_trade_previous)) * \
                               (frustrated_count + 1)
            reset = frustrated_count == refreshPeriod
            alphaDiff_v = np.where(reset, frustrated_trade, alphaDiff_v)
            frustrated_trade_previous = frustrated_trade * np.logical_not(reset)
            frustrated_count[reset] = 0
            
            result[di] = result[di - 1] + alphaDiff_v

    return result 

def barrierRefreshDecay(alpha_m, barrierValue, bookSize=None, universe_m=None, backdays=1, noiseFactor=0., refreshPeriod=3):
    assert refreshPeriod > 0
    assert noiseFactor >= 0
    if universe_m is None:
        valid = np.isfinite(alpha_m)
    else:
        valid = np.logical_and(universe_m, np.isfinite(alpha_m))

    result = alpha_m.copy()
    frustrated_trade = np.zeros((1, result.shape[1]))
    frustrated_trade_previous = frustrated_trade.copy()
    frustrated_count = np.zeros_like(frustrated_trade, dtype=np.int)
    for di in xrange(result.shape[0]):
        if di >= backdays:
            alphaDiff_v = result[di] - result[di - 1]
            tempValid_v = np.logical_or(valid[di], valid[di - 1])
            if bookSize is None:
                barrierDollarSize = np.abs(barrierValue * np.sum(np.fabs(result[di, tempValid_v])))
            else:
                barrierDollarSize = abs(barrierValue * bookSize)
            buyIx_v = np.logical_and(alphaDiff_v >= barrierDollarSize, tempValid_v)
            sellIx_v = np.logical_and(alphaDiff_v <= -barrierDollarSize, tempValid_v)
            noiseIx_v = np.logical_and(~np.logical_or(buyIx_v, sellIx_v), tempValid_v)
            frustrated_trade = noiseIx_v * alphaDiff_v
            alphaDiff_v[noiseIx_v] *= noiseFactor
            alphaDiff_v[buyIx_v] -= barrierDollarSize
            alphaDiff_v[sellIx_v] += barrierDollarSize
            
            # allow persisting small trade
            frustrated_count = (frustrated_trade != 0) * \
                               (np.sign(frustrated_trade) == np.sign(frustrated_trade_previous)) * \
                               (frustrated_count + 1)
            refreshTF = frustrated_count >= refreshPeriod
            decay = frustrated_count - refreshPeriod
            alphaDiff_v[ refreshTF[0] ] = frustrated_trade[ refreshTF[0] ] * (1 - np.exp(-decay[ refreshTF ] ))
            #alphaDiff_v[ refreshTF ] = frustrated_trade[ refreshTF ]/ decayDays
            frustrated_trade_previous = frustrated_trade * np.logical_not(refreshTF)
            
            result[di] = result[di - 1] + alphaDiff_v

    return result 

def apply_barrier_simple(mtpm_v, prev_mtpm_v, round_factor=0.1):
    
    delta_v = mtpm_v -  prev_mtpm_v

    # smaller than barrier -- no trade
    noisemask_v = (np.fabs(delta_v) < np.fabs(prev_mtpm_v) * round_factor)
    delta_v[noisemask_v] = 0.

    # buying trade larger than barrier - abs reduce by barrier amount
    buymask_v = (np.fabs(delta_v) >= np.fabs(prev_mtpm_v) * round_factor)
    buymask_v = buymask_v & (delta_v > 0.)
    delta_v[buymask_v] -= np.fabs(prev_mtpm_v[buymask_v]) * round_factor

    # selling trade larger than barrier - abs reduce by barrier amount
    sellmask_v = (np.fabs(delta_v) >= np.fabs(prev_mtpm_v) * round_factor)
    sellmask_v = sellmask_v & (delta_v < 0)
    delta_v[sellmask_v] += np.fabs(prev_mtpm_v[sellmask_v]) * round_factor

    # get implied positions
    mtpm_v = delta_v + prev_mtpm_v
    
    return mtpm_v

def barrier_pos(alpha_m, round_factor = 0.1):
    
    for i in range(1, len(alpha_m)):
        alpha_m[i] = apply_barrier_simple(alpha_m[i], alpha_m[i-1], round_factor)
    
    return alpha_m
            
def stickyGroup(group, stickyDays=2):
    
    result = np.zeros(group.shape).astype(float)
    result[:,:] = np.nan
    sticky_v = np.zeros(group.shape, dtype=int)
   
    for di in xrange(1,result.shape[0]):
        result[di] = result[di - 1] 
        for ii in xrange(result.shape[1]):
            if group[di,ii] == group[di-1,ii]:
                sticky_v[di,ii] = sticky_v[di - 1,ii] + 1
        s = sticky_v[di]
        result[di, s >= stickyDays] = group[di, s >= stickyDays] 
                     
    return result
    
def volume_constraint(alpha_m, adv_m, pr_thres = 0.1):
	assert alpha_m.shape == adv_m.shape
	thres_m = adv_m * pr_thres
	alpha_m = alpha_m.copy()
	for di in range(1, len(alpha_m)):
		trade_v = alpha_m[di] - alpha_m[di - 1]
		thres_v = thres_m[di]
		# conditions:
		# 1. delta < -constraint, the new position shall be tpm[di-1] - constraint
		# 2. delta > constraint, the new position shall be tpm[di-1] + constraint
		trade_v[np.bitwise_and(trade_v > 0, trade_v > thres_v)] =  thres_v[np.bitwise_and(trade_v > 0, trade_v > thres_v)]
		trade_v[np.bitwise_and(trade_v < 0, trade_v < -thres_v)] =  -thres_v[np.bitwise_and(trade_v < 0, trade_v < -thres_v)]
		alpha_m[di] = alpha_m[di - 1] + trade_v
	return alpha_m
    
#======================Performance Analysis and Visualization ===========================================================================#


def maxDrawDown(pnl_v):
    total_len = len(pnl_v)
    equity = pnl_v.cumsum()
    equity_max = -999
    MDD = -999
    for k in range(total_len):
        equity_max = np.maximum(equity_max,equity[k])
        DD = equity_max - equity[k]
        MDD = np.maximum(MDD,DD)
    return MDD

def maxDrawDownPct(pnl_v, bookSize):
    total_len = len(pnl_v)
    equity = pnl_v.cumsum()
    equity_max = -999
    MDD = -999
    for k in range(1,total_len):
        equity_max = np.maximum(equity_max,equity[k])
        DD = equity_max - equity[k]
        MDD = np.maximum(MDD,DD)
        MDD_pct = MDD/ bookSize[k-1] * 100
    return MDD

def alphaConcentration(alpha_m, percentile = 0.5):
    bookConcent = np.zeros(alpha_m.shape[0])

    for di in xrange(alpha_m.shape[0]):
        alpha_v = alpha_m[di]
        bookSize = np.sum(np.fabs(alpha_v))
        alpha_v_desc = np.fabs(alpha_v[np.argsort(np.fabs(alpha_v))[::-1]])/bookSize
        alpha_v_desc = alpha_v_desc[alpha_v_desc>0]
        cumBook = 0
        for cnt, alpha_s in enumerate(alpha_v_desc):
            cumBook += alpha_s
            if cumBook > percentile: 
                bookConcent[di] = cnt+1
                break
    return np.median(bookConcent)

def pnlGraph(pnl_v, label_v = False, bookSize=20e6, xLabel = 'Time', yLabel = 'Return', xTickPos = [], xTickLabel = [], title = '', legendTitle = '', loc = None):
    fig = plt.figure()
    ax = plt.subplot(111)
    for pi, pnl in enumerate(pnl_v):
        ax.plot(pnl.cumsum()/bookSize,label=label_v[pi])
    ax.set_title(title)
    ax.legend(loc='best', bbox_to_anchor=(1, 0.5),title=legendTitle)
    ax.set_xlabel(xLabel)
    ax.set_ylabel(yLabel)
    ax.xaxis.grid(True, which='major')
    #ax.yaxis.grid(True, which='major')
    ax.set_xlim([0,len(pnl_v[0])])
    if len(xTickPos) >0:
        plt.xticks(xTickPos,xTickLabel)
    if loc is not None:
        plt.axvline(loc, color='r')
    plt.show()   


def pnlGraph_m(pnl_m, label_v = False, bookSize=20e6, xLabel = 'Time', yLabel = 'Return', xTickPos = [], xTickLabel = [], legendTitle = '', title = ''):
    fig = plt.figure()
    ax = plt.subplot(111)
    for i in range(pnl_m.shape[0]):
        pnl_v = [pnl_m[i]]
        for pi, pnl in enumerate(pnl_v):
            ax.plot(pnl.cumsum()/bookSize,label=label_v[i])
    ax.set_title(title)
    ax.legend(loc='best', bbox_to_anchor=(1, 0.5),title=legendTitle)
    ax.set_xlabel(xLabel)
    ax.set_ylabel(yLabel)
    ax.xaxis.grid(True, which='major')
    #ax.yaxis.grid(True, which='major')
    ax.set_xlim([0,len(pnl_v[0])])
    if len(xTickPos) >0:
        plt.xticks(xTickPos,xTickLabel)
    plt.show()


class alphaStat(object):
    def __init__(self, alpha_m, pnl_v, tvr_v, dates_v, bookSize=20e6):
        self.minBook = np.amin( np.sum( np.fabs( alpha_m), axis = 1 ))
        self.maxPos = np.amax( np.amax( np.fabs( alpha_m ), axis = 1 ) ) / bookSize * 100
        self.IR = np.mean( pnl_v) / np.std( pnl_v )
        self.annualReturn = np.mean( pnl_v ) / bookSize * 250 * 100
        #self.longReturn = np.mean( pnl_long_v ) / (bookSize/2) * 250 * 100
        #self.shortReturn = np.mean( pnl_short_v ) / (bookSize/2) * 250 * 100
        self.MDD = maxDrawDown( pnl_v ) * 100 / bookSize
        self.turnover = np.mean( tvr_v ) * 100
        self.concent50 = alphaConcentration(alpha_m, percentile = 0.5)
        self.concent90 = alphaConcentration(alpha_m, percentile = 0.9)
        self.winning_pct = 1.* np.sum(pnl_v > 0) / len(pnl_v) * 100
        #self.ave_win = np.sum(pnl_v[pnl_v > 0]) / len(pnl_v[pnl_v > 0])
        #self.ave_loss = np.sum(pnl_v[pnl_v < 0]) / len(pnl_v[pnl_v < 0])
    def getResult(self):
        return [self.IR, self.annualReturn, self.MDD, self.turnover, self.concent50, self.concent90, self.winning_pct]
    

def alphaStatSummary(alpha_m, pnl_v, tvr_v, dates_v, bookSize=20e6, isAnnualSummary = True, loc = None, plot=True):
    years = np.unique(dates_v/10000)
    xTickPos = np.zeros(len(years))
    for yi, year in enumerate(years):
        xTickPos[yi] = np.where(dates_v == dates_v[dates_v > year*10000][0])[0]
    #pnlGraph([pnl_v],[''], xTickPos = xTickPos, xTickLabel = years)
    segDates = [dates_v/10000 == years[yi] for yi in range(len(years))]
    segAlpha = [ alpha_m[segDates[yi]] for yi in range(len(years))]
    segPnl = [ pnl_v[segDates[yi]] for yi in range(len(years))]
    segTvr = [ tvr_v[segDates[yi]] for yi in range(len(years))]
    #segPnll = [ pnl_long_v[segDates[yi]] for yi in range(len(years))]
    #segPnls = [ pnl_short_v[segDates[yi]] for yi in range(len(years))]
    
    yearlyStat = [alphaStat( segAlpha[si], segPnl[si], segTvr[si], segDates[si], bookSize = bookSize) for si in range(len(years))]
    
    result = np.zeros([len(years)+1,7])
    for yi in range(len(years)):
        result[yi] = yearlyStat[yi].getResult()
    result[len(years)] = alphaStat( alpha_m, pnl_v, tvr_v, dates_v, bookSize = bookSize).getResult()
    if plot == True:
		message = 'IR = %.3f , Return = %.2f, MDD = %.2f, TVR = %.2f' %(result[len(years),0], result[len(years),1], result[len(years),2], result[len(years),3])
		pnlGraph([pnl_v],[''], xTickPos = xTickPos, xTickLabel = years,title=message, loc = loc)
    print '\033[0m'
    pd.options.display.float_format = '{:,.2f}'.format
    resultTable = pd.DataFrame(result, index=np.append(years,'Total'), columns=['IR','Return(%)','MDD(%)', 'Turnover(%)', 'Concent50', 'Concent90','Winning(%)'])
    resultTable['IR'] = resultTable['IR'].map('{:,.3f}'.format)
    resultTable['Concent50'] = resultTable['Concent50'].map('{:,.0f}'.format)
    resultTable['Concent90'] = resultTable['Concent90'].map('{:,.0f}'.format)
    resultTable['Winning(%)'] = resultTable['Winning(%)'].map('{:,.2f}'.format)
    
    print resultTable
    
def alphaStats(alpha_m, pnl_v, tvr_v, dates_v, bookSize=20e6, isAnnualSummary = True):
    
    years = np.unique(dates_v/10000)
    xTickPos = np.zeros(len(years))
    for yi, year in enumerate(years):
        xTickPos[yi] = np.where(dates_v == dates_v[dates_v > year*10000][0])[0]
    #pnlGraph([pnl_v],[''], xTickPos = xTickPos, xTickLabel = years)
    segDates = [dates_v/10000 == years[yi] for yi in range(len(years))]
    segAlpha = [ alpha_m[segDates[yi]] for yi in range(len(years))]
    segPnl = [ pnl_v[segDates[yi]] for yi in range(len(years))]
    segTvr = [ tvr_v[segDates[yi]] for yi in range(len(years))]    
    
    yearlyStat = [alphaStat( segAlpha[si], segPnl[si], segTvr[si], segDates[si], bookSize = bookSize) for si in range(len(years))]
    
    result = np.zeros([len(years)+1,7])
    for yi in range(len(years)):
        result[yi] = yearlyStat[yi].getResult()
    result[len(years)] = alphaStat( alpha_m, pnl_v, tvr_v, dates_v, bookSize = bookSize).getResult()
    pd.options.display.float_format = '{:,.2f}'.format
    resultTable = pd.DataFrame(result, index=np.append(years,'Total'), columns=['IR','Return(%)','MDD(%)', 'Turnover(%)', 'Concent50', 'Concent90','Winning(%)'])
    resultTable['IR'] = resultTable['IR'].map('{:,.3f}'.format)
    resultTable['Concent50'] = resultTable['Concent50'].map('{:,.0f}'.format)
    resultTable['Concent90'] = resultTable['Concent90'].map('{:,.0f}'.format)
    resultTable['Winning(%)'] = resultTable['Winning(%)'].map('{:,.2f}'.format)
    return resultTable


def hist(data,bins=10):
    da = np.nan_to_num(data)
    plt.hist(da, bins)
    plt.title("Histogram")
    plt.xlabel("Data")
    plt.ylabel("Frequency")
    plt.grid(True)
    plt.show() 
    
#========================================================================================================================================#    
    
    
#======================Fast Neutralization ==============================================================================================#

@jit(nopython=True)
def chash(x):
    x = int(x)
    x = ((x >> 16) ^ x) * 0x45d9f3b
    x = ((x >> 16) ^ x) * 0x45d9f3b
    x = (x >> 16) ^ x
    return int(x)


@jit(nopython=True)
def hash_table_bucket_size(item_size):
    # from 2^6 to 2^31
    bucket_size = [53, 97, 193, 389, 769, 1543, 3079, 6151, 12289, 24593, 49157, 98317, 196613, 393241, 786433,
                   1572869, 3145739, 6291469, 12582917, 25165843, 50331653, 100663319, 201326611, 402653189, 805306457,
                   1610612741]

    bucket_num = np.ceil(np.log(item_size) / np.log(2))
    if bucket_num > 31:
        return -1
    bucket_num = bucket_num - 5 if bucket_num >= 5 else 0
    return bucket_size[int(bucket_num)]


@jit(nopython=True)
def hash_table_create(shape):
    hash_table = np.zeros(shape, np.float64)
    hash_table[:] = np.nan
    return hash_table


@jit(nopython=True)
def hash_table_update(hash_table, key, value_idx, value):
    bucket_size = hash_table.shape[0]
    list_size = hash_table.shape[1]
    bucket_num = chash(key) % bucket_size
    for bi in xrange(list_size):
        if hash_table[bucket_num, bi, 0] == key:
            hash_table[bucket_num, bi, value_idx] = value
            return True
        elif np.isnan(hash_table[bucket_num, bi, 0]):
            hash_table[bucket_num, bi, 0] = key
            hash_table[bucket_num, bi, value_idx] = value
            return True
    return False


@jit(nopython=True)
def hash_table_get(hash_table, key, value_idx, default_value):
    bucket_size = hash_table.shape[0]
    list_size = hash_table.shape[1]
    bucket_num = chash(key) % bucket_size
    for bi in xrange(list_size):
        if hash_table[bucket_num, bi, 0] == key:
            return hash_table[bucket_num, bi, value_idx]
        elif np.isnan(hash_table[bucket_num, bi, 0]):
            break
    return default_value


@jit(nopython=True)
def groupNeutralize_fast(sig, unv, grp, unique_grp, out):
    date_size = sig.shape[0]
    inst_size = sig.shape[1]
 
    unique_grp_size = len(unique_grp)
    # For safety to avoid hash collision
    hash_table = hash_table_create((hash_table_bucket_size(unique_grp_size), unique_grp_size, 2,))
 
    gi = 0
    for gi in xrange(unique_grp_size):
        if not hash_table_update(hash_table, unique_grp[gi], 1, gi):
            break
    if gi < unique_grp_size - 1:
        return False
 
    sum = np.zeros(unique_grp_size, np.float64)
    cnt = np.zeros(unique_grp_size, np.float64)
    mean = np.zeros(unique_grp_size, np.float64)
    idx = np.zeros(inst_size, np.int64)
 
    error = False
    for di in xrange(date_size):
        for ii in xrange(inst_size):
            if unv[di, ii] != 1 or np.isnan(grp[di, ii]):
                out[di, ii] = np.nan
                idx[ii] = -1
                continue
            gi = int(hash_table_get(hash_table, grp[di, ii], 1, -1))
            if gi < 0:
                error = True
                break
            idx[ii] = gi
            sum[gi] += sig[di, ii]
            cnt[gi] += 1.0
        if error:
            break
 
        for gi in xrange(unique_grp_size):
            if cnt[gi] > 0.5:
                mean[gi] = sum[gi] / cnt[gi]
                sum[gi] = 0.0
                cnt[gi] = 0.0
 
        for ii in xrange(inst_size):
            gi = idx[ii]
            if gi < 0:
                continue
            out[di, ii] = sig[di, ii] - mean[gi]
    return not error

#========================================================================================================================================#


#======================Graham Schmidt Neutralization=====================================================================================#

@numba.jit(nopython=True)
def project(v1, v2):
    """
    Project v1 to v2.
    """
    v2_norm = np.float32(0.0)
    v1_v2_product = np.float32(0.0)
    for ii in range(0, len(v2)):
        if np.isfinite(v2[ii]):
            v2_norm += v2[ii] * v2[ii]
            v1_v2_product += v1[ii] * v2[ii]
    if np.isfinite(v2_norm) & np.isfinite(v1_v2_product):
        if v2_norm != 0:
            proj_v1_onto_v2 = float(v1_v2_product) / float(v2_norm) * v2
        else: 
            proj_v1_onto_v2 = v2*np.float32(0.0)
        return proj_v1_onto_v2
    else:
        return v2*np.float32(0.0)


@numba.jit(nopython=True)
def orthogonal(v1, v2, eta = 1.0):
    """
    Extract orthogonal part of v1 to v2.
    """
    
    return v1 - project(v1, v2) * eta


@numba.jit(nopython=True)
def orthogonal_v_m(v1, m2, eta):
    """
    Extract orthogonal part of v1 to a set of vectors m2. The vector's length is the same with the length of
    the matrix's second dimension.
    """
    
    if len(v1) != len(m2[0]):
        print "Error, the dimention of inputs are not correct. Length of first input:", len(
            v1), " length of second input's second dimension:", len(m2[0])
    orth_v = v1
    for ii in range(0, len(m2)):
        orth_v = orthogonal(orth_v, m2[ii], eta = eta[ii])
    return orth_v

@numba.jit(nopython=True)
def normalize(v):
    """
    Normalize one vector
    """
    norm = np.float64(0.0)
    for ii in range(0, len(v)):
        # if np.isfinite(v[ii]):
        norm += v[ii] ** 2
    return v / (norm)  # np.sqrt for migration


@numba.jit(nopython=True)
def normalize_m(m):
    """
    Normalize one matrix in row basis, each row is a vector.
    """
    for ii in range(0, len(m)):
        m[ii] = normalize(m[ii])  # THIS for migration
        # m[ii] /= np.sqrt(np.dot(m[ii], m[ii]))
    return m

@numba.jit
def norm_svd(input_m, threshold=0.9, isnanzero=True):
    """
    Normalize and then svd.
    input_m shape need to be ri * di * ii where ri is number of risk factors
    numpy svd is not supported by Numba.
    """
    norm_res = normalize_m(input_m)
    if isnanzero == True:
        norm_res[np.isnan(norm_res)] = 0.0
    (u, s, v) = np.linalg.svd(norm_res.T, full_matrices=False)
    s_square = s * s

    norm_s_square = s_square / s_square.sum()
    reduced_columns = 0.0
    sumvar = 0.0
    for var in norm_s_square:
        sumvar += var
        reduced_columns += 1
        if sumvar > threshold:
            break

    if reduced_columns == 0:
        print "Error, reduced to an empty matrix"
    reduced = np.dot(u[:, 0:reduced_columns], np.diag(s[0:reduced_columns]))

    return reduced

@numba.jit(nopython=True)
def gs(X):
    Y = np.zeros(X.shape)
    for i in range(Y.shape[0]):
        if i==0:
            Y[i] = X[i]
        elif i==1:
            Y[i] = orthogonal(X[i],Y[i-1])
        else:
            Y[i] = orthogonal_v_m(X[i], Y[0:i],eta = np.ones(i))
    
    return Y

@numba.jit(nopython=True)
def gs_neutralization(alpha_m, F, eta):
    
    v = gs(F)
    neu_alpha = orthogonal_v_m(alpha_m, v, eta = eta)
    
    return neu_alpha 

def groupFactorsNeutralization(alpha_m, universe_m, factor_m, group_m = None, delay = 0, minGroupSize = 4, mergeSmallGroups = False, excludeZero = True, eta = None, svd = False, svd_threshold = 0.9):
    
    neutAlpha_m = np.copy( alpha_m )
    
    if eta is None:
        eta = np.ones(factor_m.shape[0])
    
    if universe_m is None:
        valid = np.isfinite( neutAlpha_m )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( neutAlpha_m ) )

    if excludeZero == True:
        valid = np.bitwise_and( valid, neutAlpha_m != 0 ) # no signal
    
    if group_m is None:
        print 'there is no group'
        group_m = valid.astype('int32')
    
    groupValid_m = np.copy(group_m)
    groupValid_m[ ~valid ] = -1
    
    for di, dailyAlpha in enumerate( neutAlpha_m ):
        
        dailyFactor_m = factor_m[ :, di, : ]
        validFactor = np.copy(valid[ di, : ])
        validFactor[:] = True
        for fi, factor in enumerate(dailyFactor_m):
            validFactor = np.bitwise_and(validFactor, np.isfinite(factor))
        dailyUniv = np.bitwise_and(valid[ di, : ], validFactor)
        groupDelay = delay if di > 0 else 0
        dailyGroup = group_m[ di - groupDelay, : ]
        
        uniqueGroup_v = np.unique( dailyGroup )
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[ uniqueGroup_v >= 0 ]

#         if mergeSmallGroups:
        smallGroupIx_v = np.copy( dailyUniv )
        smallGroupIx_v[:] = False

        for gi, groupIx in enumerate( uniqueGroup_v ):
            groupPick = np.bitwise_and( dailyUniv, dailyGroup == groupIx )
            if np.sum(groupPick) >= minGroupSize:
                F = dailyFactor_m[ :, groupPick ]
                if svd:
                    norm_F = norm_svd(F, threshold=svd_threshold).T
                    dailyAlpha[ groupPick ] = orthogonal_v_m(dailyAlpha[ groupPick ], norm_F, eta[:len(norm_F)])
                else:
                    dailyAlpha[ groupPick ] = gs_neutralization( dailyAlpha[ groupPick ], F, eta = eta )
            else:
                smallGroupIx_v[ groupPick ] = True

        if np.count_nonzero(smallGroupIx_v) > 0:
            if mergeSmallGroups:
                F = dailyFactor_m[ :, smallGroupIx_v ]
                if svd:
                    norm_F = norm_svd(F, threshold=svd_threshold).T
                    dailyAlpha[ groupPick ] = orthogonal_v_m(dailyAlpha[ groupPick ], norm_F, eta[:len(norm_F)])
                else:
                    dailyAlpha[ smallGroupIx_v ] = gs_neutralization( dailyAlpha[ smallGroupIx_v ], F, eta = eta )
            else:
                dailyAlpha[ smallGroupIx_v ] = np.nan # ignore/ NaN postions in small-sized group 
        
        dailyAlpha[~dailyUniv] = 0.0
        neutAlpha_m[ di ] = dailyAlpha

    neutAlpha_m[~valid] = 0.0
    neutAlpha_m[~np.isfinite(group_m)] = 0.0
    neutAlpha_m[~np.isfinite(neutAlpha_m)] = 0.0    
        
    return neutAlpha_m
    
    

#========================================================================================================================================

@numba.jit
def power_const_matrix(a, b_m):
    result_m = np.copy(b_m) * np.nan
    for i in range(b_m.shape[0]):
        for j in range(b_m.shape[1]):
            result_m[i, j] = a**b_m[i,j]
    return result_m

@numba.jit
def power_matrix_matrix(a_m, b_m):
    assert np.shape(a_m) == np.shapre(b_m)
    result_m = np.copy(a_m) * np.nan
    for i in range(a_m.shape[0]):
        for j in range(a_m.shape[1]):
            result_m[i, j] = a_m[i,j]**b_m[i,j]
    return result_m

#=============== feature scaling===================================================================================================#

def featureScale(X, fillna = False):
    
    if fillna:
        x = np.nan_to_num(X)
    else:
        x = X
    
    x = (x - np.nanmin(X))/(np.nanmax(X) - np.nanmin(X))
    
    return x

def groupFScale( alpha_m, universe_m = None, group_m = None, minGroupSize = 4, mergeSmallGroups = False, delay = 0, excludeZero = False ):
    # unity-based normalization within each group (for example group by industry)
    # if some group has less than minGroupSize, merge them together and unity-based normalization

    result = np.copy( alpha_m )
    if universe_m is None:
        valid = np.isfinite( result )
    else:
        valid = np.bitwise_and( universe_m, np.isfinite( result ) ) # exclude data not in the universe and nan data in alpha
    
    if excludeZero == True:
        valid = np.bitwise_and( valid, result != 0.0 ) # exclude zero data as no signal

    if group_m is None:
        print 'there is no group'
        group_m = valid.astype('int32')

    groupValid_m = np.copy(group_m)
    groupValid_m[ ~valid ] = -1

    for di, dailyAlpha in enumerate( result ):
        dailyUniv = valid[di, :]
        groupDelay = delay if di > 0 else 0
        dailyGroup = groupValid_m[di - groupDelay, :]
        
        uniqueGroup_v = np.unique(dailyGroup)
        uniqueGroup_v = uniqueGroup_v[np.isfinite(uniqueGroup_v)]
        uniqueGroup_v = uniqueGroup_v[uniqueGroup_v >= 0]

        if mergeSmallGroups:
            smallGroupIx_v = np.copy(dailyUniv)
            smallGroupIx_v[:] = False

        for gi, groupIx in enumerate(uniqueGroup_v):
            groupPick = np.bitwise_and(dailyUniv, dailyGroup == groupIx)
            if mergeSmallGroups:
                if np.sum(groupPick) >= minGroupSize:
                    dailyAlpha[groupPick] = featureScale( dailyAlpha[groupPick] )
                else:
                    smallGroupIx_v[groupPick] = True
            else: 
                dailyAlpha[groupPick] = featureScale( dailyAlpha[groupPick] )
            
        if mergeSmallGroups and np.count_nonzero(smallGroupIx_v) > 0:
            dailyAlpha[smallGroupIx_v] = featureScale( dailyAlpha[smallGroupIx_v] )
            
        result[di] = dailyAlpha
    
    result[~valid] = np.nan
    result[~np.isfinite(group_m)] = np.nan
        
    return result

#=======================miscellanoues==========================================================================#

@jit
def t_rank_n(a):
    a_v = np.copy(a)
    a_r1 = scipy.stats.rankdata(a_v)
    a_r2 = ~np.isnan(a_v) & ~np.isinf(a_v)
    a_r1 = a_r1*a_r2
    a_r1 = a_r1/ np.max(a_r1[~np.isnan(a_r1) & ~np.isinf(a_r1)])
    a_r1[a_r1==0.]=np.nan
    return a_r1    

def rank_alpha(input_v):
    a = np.copy(input_v)
    for i in range(len(a)):
        if (len(a[i]) == np.sum(np.isnan(a[i]))): 
            continue
        else:
            a[i] = t_rank_n(a[i])
    return a

def r(input_v,univ):
    input_v[~univ] = np.nan
    a = np.copy(input_v)
    for i in range(len(a)):
        if (len(a[i]) == np.sum(np.isnan(a[i]))): 
            continue
        else:
            a[i] = t_rank_n(a[i])
    return a  

def quantile(alpha_rank,lower=0.0,upper=1.0,type='tail', equal_weight = True, zero = False):
    
    result = np.copy(alpha_rank)
    if (type == 'tail'):
        if zero:
            result[ ((alpha_rank >= lower) & (alpha_rank < upper))] = 0.
        else:
            result[ ((alpha_rank >= lower) & (alpha_rank < upper))] = np.nan
        if equal_weight:
            result[alpha_rank < lower] = -1
            result[alpha_rank >= upper] = 1
    else:
        if zero:
            result[ ~((alpha_rank >= lower) & (alpha_rank < upper))] = 0.
        else:
            result[ ~((alpha_rank >= lower) & (alpha_rank < upper))] = np.nan
        if equal_weight:
            result[np.bitwise_and(alpha_rank >= lower, alpha_rank < 0.5)] = -1
            result[np.bitwise_and(alpha_rank <= upper, alpha_rank >= 0.5)] = 1
    
    return result

def winsorizes(input_m,universe_m,limit=0.05):
    input_n = np.copy(input_m)
    univ_n = np.copy(universe_m)
    rr = r(input_n,univ_n)
    input_n[rr > (1-limit/2.0)]=np.nan
    input_n[rr < (limit/2.0)]=np.nan
    return input_n

def stopLoss(alpha_m,return_m,day=1,thres=-0.03):
    alpha_a = np.copy(alpha_m)
    ret = tsOp.fastMProd(return_m, day)
    ret[alpha_a < 0] *= -1.0
    
    for i in range(len(alpha_a)):
        alpha_a[i,:]=(ret[i,:] >= thres)*alpha_a[i,:]
    alpha_a[alpha_a==0]=np.nan
    return alpha_a

def cut_group(alpha_m,group_m,return_m,days=100,days2=40,ir_thred=0.00, aabs=False):
    gr = np.unique(group_m)
    k = np.sum(~np.isnan(gr))
    gr = gr[:k]
    alpha_n = np.copy(alpha_m)
    return_n = np.copy(return_m)[2:]
    return_n = np.pad(return_n, ((0,2), (0,0)), 'edge')
    # gr_pnl = []
    ir_t=[]
    for i in range(len(gr)):
        r = return_n[:,group_m[0,:] == gr[i]]
        a = alpha_n[:,group_m[0,:] == gr[i]]
        p = np.nan_to_num(r*a)
        pnl = np.nan_to_num(np.sum(p, axis=1))
        # print pnl.shape
        # print pnl

        for j in range(days,len(alpha_n)):
            ir_temp = np.mean(pnl[j-days:j]) /np.std(pnl[j-days:j])
            ir_temp2 = np.mean(pnl[j-days2:j]) /np.std(pnl[j-days2:j])
            if (aabs):
                ir_temp = abs(ir_temp)
                ir_temp2 = abs(ir_temp2)
            # if (j==1200):
                # ir_t.append(ir_temp)
        if ( ((ir_temp) < ir_thred ) & ((ir_temp2) < ir_thred ) ) :
            alpha_n[j,group_m[0,:]== gr[i]] = np.nan
    # print np.nanmean((ir_t))
    # print ((ir_t))
    return alpha_n

def hump(alpha_m,ratio=0.1):
    alpha_v = np.copy(alpha_m)
    alpha_v = np.nan_to_num(alpha_v)
    for i in range(1,len(alpha_v)):
        change = alpha_v[i,:] - alpha_v[i-1,:]
        delta = np.fabs(change / alpha_v[i-1,:])
        alpha_v[i,:] = ((delta > ratio)*alpha_v[i,:] + (delta < ratio)*alpha_v[i-1,:] )
    # alpha_v[alpha_v==0] = np.nan
    return alpha_v

def hump_new(alpha_m,dates_v,ratio=0.1,refresh=1):
    alpha_v = np.copy(alpha_m)
    dates = np.copy(dates_v)
    alpha_v = np.nan_to_num(alpha_v)
    for i in range(1,len(alpha_v)):
        if (dates[i]-dates[i-1]<50*refresh):
            change = alpha_v[i,:] - alpha_v[i-1,:]
            delta = np.fabs(change / alpha_v[i-1,:])
            alpha_v[i,:] = ((delta > ratio)*alpha_v[i,:] + (delta < ratio)*alpha_v[i-1,:] )
    # alpha_v[alpha_v==0] = np.nan
    return alpha_v

def corr(df1,df2):
    
    pnl1 = df1['Pnl'].values
    di1 = df1['Date'].values
    pnl2 = df2['Pnl'].values
    di2 = df2['Date'].values
    
    if di1[0] > di2[0]:
        ix = np.where(di2 == di1[0])
        if len(ix[0]) == 0:
            print 'Error'
            return
        di2 = di2[ix[0][0]:]
        pnl2 = pnl2[ix[0][0]:]
    elif di1[0] < di2[0]:
        ix = np.where(di1 == di2[0])
        if len(ix[0]) == 0:
            print 'Error'
            return
        di1 = di1[ix[0][0]:]
        pnl1 = pnl1[ix[0][0]:]
    
    if di1[-1] > di2[-1]:
        ix = np.where(di1 == di2[-1])
        if len(ix[0]) == 0:
            print 'Error'
            #return
        di1 = di1[:ix[0][0]+1]
        pnl1 = pnl1[:ix[0][0]+1]
    elif di1[-1] < di2[-1]:
        ix = np.where(di2 == di1[-1])
        if len(ix[0]) == 0:
            print 'Error'
            #return
        di2 = di2[:ix[0][0]+1]
        pnl2 = pnl2[:ix[0][0]+1]    
    
    result = np.corrcoef(pnl1, pnl2)[0][1]
    
    return result
    
       
   # if len(pnl1) > len (pnl2):
#        for i in range(len(pnls)):
#            if (days[i] == days_2[0] ):
#                pnls=pnls[i:]
#                break 
#        for i in range(len(pnls)-1,0,-1):
#            if (days[i] == days_2[-1] ):
#                pnls=pnls[:i]
#                break 
#    if len(pnls_2) > len (pnls):
#        for i in range(len(pnls_2)):
#            if (days_2[i] == days[0] ):
#                pnls_2=pnls_2[i:]
#                break
#        for i in range(len(pnls_2)-1,0,-1):
#            if (days_2[i] == days[-1] ):
#                pnls_2=pnls_2[:i]
#                break 
#
#    for i in range(len(pnls)-1):
#        pnls[i] = pnls[i+1] - pnls[i]
#        pnls_2[i] = pnls_2[i+1] - pnls_2[i]
#    pnls=pnls[:-1]
#    pnls_2=pnls_2[:-1]
#    

def test_corr(pnl, dates):
    pnl1 = np.copy(pnl)
    pnl1 = pnl1[np.where(pnl1!=0)]

    d = np.copy(dates)
    d = d[np.where(pnl1!=0)]

    df = np.vstack([d,pnl1])
    df = df.T
    df = pd.DataFrame(df,columns=['Date','Pnl'])

    dff = []
    strgs = []
    for root, dirs, files, in os.walk("$HOME/work/Submit/_pnl/"):
        for file in files:
            if file.endswith(".pkl"):
                strg = "$HOME/work/Submit/_pnl/" + file
                dff = pd.read_pickle(strg)
                x = corr(dff, df)
                if x > 0.5:
                    print strg
                    print x

def plot_coverage(x, univS_m):
    plt.plot(1.*np.sum(np.bitwise_and(univS_m, np.isfinite(x)),axis=1)/ np.sum(univS_m, axis=1))
    plt.show()

#========= operations from Downy ===========================================================================#
    
@numba.jit(nopython=True)
def get_nth_revision(input_tuple):  
    
    value_m = input_tuple[0]
    fy_m = input_tuple[1]
    
    output_m = np.full_like(input_tuple[0], np.nan)
    
    for ii in xrange(value_m.shape[1]):
        current_period = np.nan
        first_value = True
        for di in xrange(value_m.shape[0]):
            value = value_m[di,ii]
            if not np.isnan(value):
                fy = fy_m[di,ii]
                if first_value or (fy > current_period):
                    nth_revision = 0
                    output_m[di,ii] = nth_revision
                    current_period = fy
                    first_value = False
                else:
                    output_m[di,ii] = nth_revision
                nth_revision += 1
    return output_m


def build_matrix_from_lv1(input_df, target_column_name, input_date_list, input_security_list):
    
    # input_df             : filtered dataframe after retrieved from get_data(level = 1)
    # target_column_name   : column name in lv1 dataframe
    # input_date_list      : date_list retrieved from hfsubunvrs
    # input_security_list  : security_list used for trim

    print "Filtering out HFSID not in universe..."
    temp = input_df[['DATE', 'HFSID', target_column_name]]
    temp = temp[temp['HFSID'].isin(input_security_list)]
    
    ii_dict = {
        hfsid:ii
        for ii, hfsid in enumerate(input_security_list)
    }
   
    print "Building matrix..."
    result_m = np.empty((len(input_date_list), len(input_security_list)))
    result_m[:] = np.nan
    
    for date, hfsid, value in temp.values:
        if date >= input_date_list[0] and date <= input_date_list[-1]:
            di = np.searchsorted(input_date_list, date)
            ii = ii_dict.get(hfsid, -1)
            if not ii == -1:
                result_m[di,ii] = value
                
    print "Done."
    return result_m 

def aggregate_matrix_from_lv1(input_df, target_column_name, input_date_list, input_security_list, method = 'add'):
    
    # input_df             : filtered dataframe after retrieved from get_data(level = 1)
    # target_column_name   : column name in lv1 dataframe
    # input_date_list      : date_list retrieved from hfsubunvrs
    # input_security_list  : security_list used for trim

    print "Filtering out HFSID not in universe..."
    temp = input_df[['DATE', 'HFSID',target_column_name]]
    temp = temp[temp['HFSID'].isin(input_security_list)]
    
    ii_dict = {
        hfsid:ii
        for ii, hfsid in enumerate(input_security_list)
    }
    
    print "Building matrix..."
    result_m = np.empty((len(input_date_list), len(input_security_list)))
    result_m[:] = np.nan
    
    for date, hfsid, value in temp.values:
        if date >= input_date_list[0] and date <= input_date_list[-1]:
            di = np.searchsorted(input_date_list, date)
            ii = ii_dict.get(hfsid, -1)
            if not ii == -1:
                if method=='add':
                    if ~np.isfinite(result_m[di,ii]):
                        result_m[di,ii] = value
                    else:
                        result_m[di,ii] += value
                elif method=='count':
                    result_m[di,ii] += 1
                elif method=='max':
                    result_m[di,ii] = np.nanmax(result_m[di,ii], value)
                
    print "Done."
    return result_m 

def reindex(arr, trim, new_trim, fill_value=np.nan):
    assert arr.shape[-1] == len(trim)

    if arr.ndim == 1:
        s = pd.Series(arr, index=trim)
        s = s.reindex(index=new_trim, fill_value=fill_value)
    elif arr.ndim == 2:
        s = pd.DataFrame(arr, columns=trim)
        s = s.reindex(columns=new_trim, fill_value=fill_value)
    else:
        raise ValueError('Invalid # dimensions of arr_m: %s' % arr.ndim)

    return s.values

#==========operations to constraint volume on holiday from Juho =============================================================#

@numba.jit(nopython=True)
def _remap(alpha, univ, booksize=20e6, balance=True, minInstNum=2, zero_out_univ=True):
    Ndi, Nii = alpha.shape
    res = np.copy(alpha)  # np.zeros(alpha.shape,np.float64)
    valid = univ & np.isfinite(alpha)
    for di in xrange(Ndi):
        cnt = 0
        sum_ii = 0.0
        dbook = 0.0
        for ii in xrange(Nii):
            if valid[di, ii]:
                dalpha = alpha[di, ii]
                sum_ii += dalpha
                dbook += abs(dalpha)
                cnt += 1
        if cnt == 0.0:
            continue
        if dbook == 0.0:
            # print "remap, zero daily booksize, di=",di
            continue
        elif cnt < minInstNum:
            # print "remap, num of instrument is too small, di=",di
            continue
        elif not balance:
            mean = sum_ii / float(cnt)
            for ii in xrange(Nii):
                if valid[di, ii]:
                    res[di, ii] -= mean
        else:
            posbook = 0.0
            negbook = 0.0
            for ii in xrange(Nii):
                if valid[di, ii]:
                    dalpha = alpha[di, ii]
                    if dalpha > 0:
                        posbook += dalpha
                    else:
                        negbook += dalpha

            if posbook == 0.0 or negbook == 0.0:
                mean = sum_ii / float(cnt)
                for ii in xrange(Nii):
                    if valid[di, ii]:
                        res[di, ii] -= mean

            else:
                for ii in xrange(Nii):
                    if valid[di, ii]:
                        dalpha = alpha[di, ii]
                        if dalpha < 0:
                            res[di, ii] = dalpha * posbook / abs(negbook)

        dbook = 0.0
        for ii in xrange(Nii):
            if valid[di, ii]: dbook += abs(res[di, ii])
        for ii in xrange(Nii):
            if valid[di, ii] and dbook != 0.0:
                res[di, ii] *= booksize / dbook
    if zero_out_univ:
        for di in xrange(Ndi):
            for ii in xrange(Nii):
                if not univ[di, ii]:
                    res[di, ii] = 0.0
    return res


@numba.jit(nopython=True)
def forwardfill_univ(data, univ):
    res = np.copy(data)
    Ndi, Nii = res.shape
    for di in xrange(Ndi):
        if di == 0:
            continue
        for ii in xrange(Nii):
            if univ[di, ii]:
                res[di, ii] = res[di - 1, ii]
    return res


def remap_holiday(alpha, holiday_univ, univ=None, bookSize=20e6, minInstNum=2):
    """
    This function is for remapped alpha as input.
    """
    assert np.shape(alpha) == np.shape(holiday_univ)
    ############## forwardwill to holiday
    alpha_holiday_ff = forwardfill_univ(alpha, holiday_univ)
    if univ is None:
        univ = np.isfinite(alpha_holiday_ff)
    return _remap_holiday(alpha_holiday_ff, holiday_univ, univ, bookSize, minInstNum)


@numba.jit(nopython=True)
def _remap_holiday(alpha, holiday_univ, univ, bookSize, minInstNum):
    Ndi, Nii = alpha.shape
    res = np.copy(alpha)
    valid = univ & np.isfinite(alpha)
    valid_hol = valid & holiday_univ
    valid_non_hol = valid & (~holiday_univ)
    for di in xrange(Ndi):
        
        if di == 0:
            continue

        dbook_holi = 0.0  # holiday booksize
        dlong_holi = 0.0  # long_exposure to holiday exchange
        cnt_hol = 0.0
        ####### holiday_univ: forwardfill d-1 dollar-volume #######
        for ii in xrange(Nii):
            if holiday_univ[di, ii]:
                prev_pos = res[di - 1, ii]
                res[di, ii] = prev_pos
                dlong_holi += prev_pos
                dbook_holi += abs(prev_pos)
                cnt_hol += 1

        if dbook_holi == 0.0:  # no holiday: pass the di
            continue

        dtarget_book = bookSize - dbook_holi  # non_holiday booksize

        dptarget = 0.5 * (bookSize - dbook_holi - dlong_holi)  # non_holiday long size
        dntarget = 0.5 * (bookSize - dbook_holi + dlong_holi)  # non_holiday short size

        dbook = 0.0
        cnt = 0.0
        for ii in xrange(Nii):
            if valid[di, ii]:
                dbook += abs(alpha[di, ii])
                cnt += 1

        if dbook == 0.0:
            print "remap, zero daily booksize, di=", di
            continue
        elif cnt < minInstNum:
            print "remap, num of instrument is too small, di=", di
            continue

        elif cnt == cnt_hol:  # if holiday = daily univ: we skip the row
            # print di
            continue

        else:
            posbook = 0.0
            negbook = 0.0
            cnt = 0.0
            for ii in xrange(Nii):
                if valid_non_hol[di, ii]:
                    dalpha = alpha[di, ii]
                    cnt += 1
                    if dalpha > 0:
                        posbook += dalpha
                    else:
                        negbook += np.abs(dalpha)

            if cnt == 0.0:
                continue

            if posbook == 0.0:  # in case only negative book: balance by mean
                mean = negbook / float(cnt)
                negbook = 0.0  # define new pos/negbook
                for ii in xrange(Nii):
                    if valid_non_hol[di, ii]:
                        res[di, ii] = alpha[di, ii] + mean
                        dres = res[di, ii]
                        if dres > 0:
                            posbook += dres
                        else:
                            negbook += abs(dres)

            elif negbook == 0.0:  # in case only positive book: balance by mean
                mean = posbook / float(cnt)
                posbook = 0.0  # define new pos/negbook
                for ii in xrange(Nii):
                    if valid_non_hol[di, ii]:
                        res[di, ii] = alpha[di, ii] - mean
                        dres = res[di, ii]
                        if dres > 0:
                            posbook += dres
                        else:
                            negbook += abs(dres)

            if posbook == 0.0 or negbook == 0.0:
                continue
            for ii in xrange(Nii):
                if valid_non_hol[di, ii]:
                    dalpha = alpha[di, ii]
                    if dalpha > 0:
                        res[di, ii] = dalpha * dptarget / posbook
                    else:
                        res[di, ii] = dalpha * dntarget / negbook

    return res

################################ ops to constraint pos on EU holiday ###############################################################

@numba.jit(nopython=True)
def ffill_holi(alpha_m, holiday_univ):
    result_m = np.copy(alpha_m)
    Ndi, Nii = result_m.shape
    for di in xrange(Ndi):
        if di == 0:
            continue
        for ii in xrange(Nii):
            if holiday_univ[di, ii]:
                result_m[di, ii] = result_m[di - 1, ii]
    return result_m

def remap_holi(alpha_m, holiday_univ, univ=None, bookSize=20e6):
    """
    This function forward fill the remapped alpha then remap again. The logic is as followed:
        1. Forward fill the positions on holiday
        2. Calculate GMV and Exposure of these ffilled positions
        3. Adjust the 'signal' positions accordingly to ensure market neutral and 20M booksize
    """
    res_m = ffill_holi(alpha_m, holiday_univ)
    if univ is None:
        univ = np.isfinite(res_m)
    else:
        valid = univ & np.isfinite(alpha_m)
    res_m[~valid] = 0.
    valid_hol = valid & holiday_univ
    valid_non_hol = valid & (~holiday_univ)
    Ndi, Nii = res_m.shape
    for di in xrange(Ndi):
        holiday_v = holiday_univ[di]
        valid_hol_v = valid_hol[di]
        valid_non_hol_v = valid_non_hol[di]
        res_v = res_m[di]
        if di == 0 or np.sum(holiday_v) == 0:
            continue
        if np.sum(~holiday_v) == 0:
            res_m[di] = res_m[di - 1]
            continue
        
        dbook_holi = np.nansum(np.fabs(res_v[holiday_v]))  # booksize for positions in holiday
        dexp_holi = np.nansum(res_v[holiday_v]) # exposure for positions in holiday
        
        dtarget_book = bookSize - dbook_holi  # non_holiday booksize

        dptarget = 0.5 * (bookSize - dbook_holi - dexp_holi)  # non_holiday long size
        dntarget = 0.5 * (bookSize - dbook_holi + dexp_holi)  # non_holiday short size
        
        posbook = np.nansum(res_v[(res_v > 0) & valid_non_hol_v])
        negbook = np.fabs(np.nansum(res_v[(res_v < 0) & valid_non_hol_v]))
        
        res_v[(res_v > 0) & valid_non_hol_v] = res_v[(res_v > 0) & valid_non_hol_v] * dptarget / posbook
        res_v[(res_v < 0) & valid_non_hol_v] = res_v[(res_v < 0) & valid_non_hol_v] * dntarget / negbook
        
        res_m[di] = res_v
    
    return res_m

################### combine different estimates #######################################################################################

def combine_mean(est_tuple_1, est_tuple_2):
    '''Please pass est tuple is in the form of estimate mean, number of estimates, date'''
    (mean_1, num_1, date_1) = est_tuple_1
    (mean_2, num_2, date_2) = est_tuple_2
    res_mean = np.zeros(mean_1.shape) * np.nan
    res_date = np.zeros(date_1.shape) * np.nan
    estimate_mean = (mean_1 + mean_2)/ 2.
    estimate_num = num_1 + num_2
    estimate_wgt_mean = (mean_1 * num_1 + mean_2 * num_2)/ estimate_num
    estimate_wgt_mean[~np.isfinite(estimate_num)] = estimate_mean[~np.isfinite(estimate_num)]
    res_mean[date_1 == date_2] = estimate_wgt_mean[date_1 == date_2]
    res_date[date_1 == date_2] = date_1[date_1 == date_2]
    res_mean[date_1 > date_2] = mean_1[date_1 > date_2]
    res_date[date_1 > date_2] = date_1[date_1 > date_2]
    res_mean[date_2 > date_1] = mean_2[date_2 > date_1]
    res_date[date_2 > date_1] = date_2[date_2 > date_1]
    res_mean[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))] = mean_1[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))]
    res_date[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))] = date_1[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))]
    res_mean[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))] = mean_2[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))]
    res_date[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))] = date_2[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))]
    return res_mean, res_date

def combine_std(est_tuple_1, est_tuple_2):
    '''Please pass est tuple is in the form of estimate stdev, number of estimates, date'''
    (std_1, num_1, date_1) = est_tuple_1
    (std_2, num_2, date_2) = est_tuple_2
    res_std = np.zeros(std_1.shape) * np.nan
    res_date = np.zeros(date_1.shape) * np.nan
    estimate_std = (std_1 + std_2)/ 2.
    estimate_num = num_1 + num_2 - 2
    estimate_pool_std = np.sqrt(((num_1 - 1) * (std_1**2) + (num_2 - 1) * (std_2**2))/ (num_1 + num_2 - 2))
    estimate_pool_std[~np.isfinite(estimate_num)] = estimate_std[~np.isfinite(estimate_num)]
    estimate_pool_std[estimate_num==0.] = estimate_std[estimate_num==0.]
    res_std[date_1 == date_2] = estimate_pool_std[date_1 == date_2]
    res_date[date_1 == date_2] = date_1[date_1 == date_2]
    res_std[date_1 > date_2] = std_1[date_1 > date_2]
    res_date[date_1 > date_2] = date_1[date_1 > date_2]
    res_std[date_2 > date_1] = std_2[date_2 > date_1]
    res_date[date_2 > date_1] = date_2[date_2 > date_1]
    res_std[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))] = std_1[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))]
    res_date[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))] = date_1[np.bitwise_and(np.isfinite(date_1),~np.isfinite(date_2))]
    res_std[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))] = std_2[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))]
    res_date[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))] = date_2[np.bitwise_and(np.isfinite(date_2),~np.isfinite(date_1))]
    return res_std, res_date

def combine_mean_1(tuple_list):
    '''Please pass a list containing tuples, each in the form of estimate mean, number of estimates, date'''
    mean = []
    num = []
    date = []
    for i in range(len(tuple_list)):
        (m, n, d) = tuple_list[i]
        mean.append(m)
        num.append(n)
        date.append(d)
    mean = np.array(mean)
    num = np.array(num)
    date = np.array(date)
    res_mean = np.zeros(mean[0].shape) * np.nan
    res_date = np.zeros(date[0].shape) * np.nan
    #estimate_wgt_mean = np.nansum(mean * num, axis=0)/ np.nansum(num, axis=0)
    for i in range(res_mean.shape[0]):
        for j in range(res_mean.shape[1]):
            mean_v = mean[:, i, j]
            num_v = num[:, i, j]
            date_v = date[:, i, j]
            loc = np.where(date_v == np.nanmax(date_v))[0]
            if len(loc)==0:
                continue
            else:
                mean_v = mean_v[date_v == np.nanmax(date_v)]
                num_v = num_v[date_v == np.nanmax(date_v)]
                res_mean[i,j] = np.nansum(mean_v * num_v)/ np.nansum(num_v)
                res_date[i,j] = date_v[loc[0]]
    return res_mean, res_date

def combine_std_1(tuple_list):
    '''Please pass a list containing tuples, each in the form of estimate std, number of estimates, date'''
    std = []
    num = []
    date = []
    for i in range(len(tuple_list)):
        (s, n, d) = tuple_list[i]
        std.append(s)
        num.append(n)
        date.append(d)
    std = np.array(std)
    num = np.array(num)
    date = np.array(date)
    res_std = np.zeros(std[0].shape) * np.nan
    res_date = np.zeros(date[0].shape) * np.nan
    #estimate_wgt_mean = np.nansum(mean * num, axis=0)/ np.nansum(num, axis=0)
    for i in range(res_std.shape[0]):
        for j in range(res_std.shape[1]):
            std_v = std[:, i, j]
            num_v = num[:, i, j]
            date_v = date[:, i, j]
            loc = np.where(date_v == np.nanmax(date_v))[0]
            if len(loc)==0:
                continue
            elif len(loc)==1:
                res_std[i,j] = std_v[loc[0]]
                res_date[i,j] = date_v[loc[0]]
            else:
                std_v = std_v[date_v == np.nanmax(date_v)]
                num_v = num_v[date_v == np.nanmax(date_v)]
                res_std[i,j] = np.sqrt(np.nansum((num_v - 1) * (std_v**2))/ np.nansum(num_v - 1))
                res_date[i,j] = date_v[loc[0]]
    return res_std, res_date

def count_unique(input_m, axis=1):
    result = []
    for i in range(input_m.shape[axis]):
        if axis==0:
            temp_v = input_m[i,:]
        else:
            temp_v = input_m[:,i]
        temp_v = temp_v[np.isfinite(temp_v)]
        temp_v = np.unique(temp_v)
        result.append(len(temp_v))
    result = np.array(result)
    return result

##########################################################################################################################################

# def groupWeightedMean( X_m, W_m, group_m, univ_m = None, fillna1 = True, fillna2 = True):
#     # calculate weighted mean of X in each group
#     # W_m indicate the weights of each elements
#     # dynamic grouping group_m
    
#     if fillna1 == True:
#         x_m = np.nan_to_num(X_m)
#     else:
#         x_m = X_m
        
#     if fillna1 == True:
#         w_m = np.nan_to_num(W_m)
#     else:
#         w_m = W_m
        
#     y_m = x_m * w_m
    
#     g = np.unique(group_m)
#     g = g[np.isfinite(g)]
#     result = np.zeros([x_m.shape[0],len(g)])
#     result = pd.DataFrame(result, columns = g)
    
#     for grp in g:
#         b_m = (group_m == grp)
#         if univ_m is not None:
#             y = np.sum(y_m * b_m * univ_m, axis=1)
#             w = np.sum(w_m * b_m * univ_m, axis=1)
#         else:
#             y = np.sum(y_m * b_m, axis=1)
#             w = np.sum(w_m * b_m, axis=1)
#         result.loc[:][grp] = np.array(y/w)
    
#     return result

# def groupCount(group_m, univ_m = None):
#     # count number of elements in group
    
#     g = np.unique(group_m)
#     g = g[np.isfinite(g)]
#     result = np.zeros([group_m.shape[0],len(g)])
#     result = pd.DataFrame(result, columns = g)
    
#     for grp in g:
#         b_m = (group_m == grp)
#         if univ_m is not None:
#             s = np.sum(b_m * univ_m, axis=1)
#         else:
#             s = np.sum(b_m, axis=1)
        
#         result.loc[:][grp] = s
        
#     return result

def univ_match(small_univ_m, small_univ_tickers, big_univ_tickers):
    univ_df = pd.DataFrame(small_univ_m, columns = small_univ_tickers)
    univ_m = univ_df.reindex(columns = big_univ_tickers).as_matrix().astype(float)
    univ_m[~np.isfinite(univ_m)] = 0.
    univ_m = univ_m.astype(bool) 
    return univ_m

def calculate_netting(alpha_m1, alpha_m2):
    # assume book size of 20mil each
    alpha_sum = alpha_m1 + alpha_m2
    net_v = np.nansum(np.fabs(alpha_sum), axis=1)/ (np.nansum(np.fabs(alpha_m1), axis=1) + np.nansum(np.fabs(alpha_m2), axis=1))
    return net_v
    
''' factor analysis ops '''

def rank_calc(factor_m, target_m, univS_m = None):
	if univS_m is None:
		univ_m = np.bitwise_and(np.isfinite(target_m), np.isfinite(factor_m))
	else:
		univ_m = univS_m.copy()
		univ_m = np.bitwise_and(univ_m, np.isfinite(target_m))
		univ_m = np.bitwise_and(univ_m, np.isfinite(factor_m))
	
	factor_rank_m = groupRank(factor_m, univ_m)
	target_rank_m = groupRank(target_m, univ_m)
	
	return factor_rank_m, target_rank_m
	
def ic_calc(factor_m, target_m, univS_m = None):
	# calculate daily rank IC between factor and target 
	factor_rank_m, target_rank_m = rank_calc(factor_m, target_m, univS_m)
	ic = []
	for i in range(len(target_rank_m)):
		x = factor_rank_m[i]
		y = target_rank_m[i]
		x = x[np.isfinite(x)]
		y = y[np.isfinite(y)]
		ic.append(np.corrcoef(x,y)[0,1])
	ic = np.array(ic)
	# plot
	plt.plot(ic)
	plt.title('daily IC')
	plt.show()
	return ic
	
def pctile_return_calc(factor_m, target_m, univS_m = None, n = 5):
	# calculate n pctile average daily return_n, return di*qi daily return matrix 
	factor_rank_m, target_rank_m = rank_calc(factor_m, target_m, univS_m)
	step = 1./n
	r_m = []
	for i in range(n):
		lb = i * step
		ub = (i + 1) * step
		q = np.bitwise_and(factor_rank_m > lb, factor_rank_m <= ub)
		q_ret = target_m.copy()
		q_ret[~q] = np.nan
		r_m.append(np.nansum(q_ret, axis=1))
	r_m = np.array(r_m).T
	y_pos = np.arange(n)
	ret = np.nanmean(r_m, axis= 0)
	plt.bar(y_pos, ret, align='center', alpha=0.5)
	plt.title('n-tiles return')
	plt.show()
	
	return r_m


###############################   fast loader ########################################################################################

def hash_items(*args):
    m = hashlib.md5()
    for v in args:
        m.update(str(v))
    
    return m.hexdigest()
    




