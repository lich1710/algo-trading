# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 2
#     language: python
#     name: python2
# ---

# +
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import alphaOp
import tsOp
import nanOp
import simulate
import cPickle as pickle
# import hf.pm.summary as summary
# -

def data_extraction(df):
    df = df.rename(columns={"<Ticker>": "Ticker", "<DTYYYYMMDD>": "Date", "<Open>": "Open","<High>": "High","<Low>": "Low","<Close>": "Close", "<Volume>": "Volume"})
    
    # Get unique tickers
    tickers = np.unique(df.Ticker.values).astype(str)
    # filter for stock ticker only
    stock_tickers = []
    for ticker in tickers:
        if len(ticker) == 3:
            stock_tickers.append(ticker)
    stock_tickers = np.array(stock_tickers)
    print 'Number of stock ticker is: ', len(stock_tickers)
    
    # Get unique dates
    dates   = np.unique(df.Date.values)
    print 'Number of unique dates is: ', len(dates)

    # Get stock df
    stock_df = df[df.Ticker.isin(stock_tickers)]

    # Define open/high/low/close/volume df for stock_ticker
    open_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Open')
    high_df = stock_df.pivot_table(index='Date', columns='Ticker', values='High')
    low_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Low')
    close_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Close')
    volume_df = stock_df.pivot_table(index='Date', columns='Ticker', values='Volume')
    
    
    # Save into a raw data dict
    raw_data_dict = {}
    raw_data_dict['Open'] = open_df
    raw_data_dict['High'] = high_df
    raw_data_dict['Low'] = low_df
    raw_data_dict['Close'] = close_df
    raw_data_dict['Volume'] = volume_df
    
    return raw_data_dict

# ### Raw data

### checking data
filename1 = 'CafeF.RAW_HSX.Upto01.11.2019.csv'
filename2 = 'CafeF.RAW_HNX.Upto01.11.2019.csv'

df1 = pd.read_csv(filename1)
df1['Exchange'] = 'HSX'
df2 = pd.read_csv(filename2)
df2['Exchange'] = 'HNX'

raw_df = df1.append(df2)

# Length of the data
len(raw_df)

raw_data_dict = data_extraction(raw_df)

pickle.dump( raw_data_dict, open( "pickle_raw_data_dict.p", "wb" ), -1 )

# ### Adjusted data

### checking data
filename1 = 'CafeF.HSX.Upto01.11.2019.csv'
filename2 = 'CafeF.HNX.Upto01.11.2019.csv'

df1 = pd.read_csv(filename1)
df1['Exchange'] = 'HSX'
df2 = pd.read_csv(filename2)
df2['Exchange'] = 'HNX'

df = df1.append(df2)

len(df)

data_dict = data_extraction(df)

pickle.dump( data_dict, open( "pickle_data_dict.p", "wb" ), -1 )
