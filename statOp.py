import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from numpy import *
import statsmodels.tsa.stattools as tss

# testing

def mAdfuller(ts, lags = 1):
    """return the p value of ADF test for multiple time series column vectors"""
    
    result = []
    for i in range(ts.shape[1]):
        res = tss.adfuller(ts[:,i])[1]
        result.append(res)
    
    result = np.array(result)
    
    return result
    
def HalfLife(ts):
    
    ts_diff = np.diff(ts, axis = 0)
    ts = ts[1:]
    coefs = np.sum((ts_diff - np.mean(ts_diff, axis=0))*(ts - np.mean(ts, axis = 0)), axis = 0)/ np.sum((ts - np.mean(ts, axis=0))**2, axis =0)
    result = np.log(2)/coefs
    return result

def Hurst(ts, lags = 20):
    
    tau = np.zeros((lags-2, ts.shape[1]))
    for lag_ix in xrange(2,lags):
        tau[lag_ix-2] = np.sqrt(np.std(ts[lag_ix:,:] - ts[:-lag_ix,:], axis = 0))  
    y = log(tau)
    x = np.log(np.array(xrange(2,lags)))
    x = np.matlib.repmat(x, y.shape[1], 1).T
    
    result = 2.*np.sum((y - np.mean(y, axis=0))*(x - np.mean(x, axis = 0)), axis = 0)/ np.sum((x - np.mean(x, axis=0))**2, axis =0)
    
    return result
    
    
def trimm(data, m=2):
    data[abs(data - np.mean(data)) > m * np.std(data)] = 0
    return data
    
    
    
    
    
   
    
        

    